<%@ page isErrorPage = "true" language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<html>
<link href="${pageContext.request.contextPath}/css/error_style.css"
	rel="stylesheet" type="text/css">
<title>Error Page 500</title>
<meta charset="UTF-8">

<body>
	<div class="page">
		<div class="form">
		 	<p class="message"><fmt:message key="error.statuscode" />: ${pageContext.errorData.statusCode} </p> 
			<p class="message"> <fmt:message key="error.info500.one" /> </p>
			<p class="message"> <fmt:message key="error.info500.two" /> </p>
			<p class="failmessage">${errorMessage}</p>

			<br/>
			<br/>
			<br/>
			<br/>
			<p class="message"> <fmt:message key="error.comeback" />:</p>
			<br/>
			<form action="${pageContext.request.contextPath}/index.jsp">
				<button type="submit" class="button"> <fmt:message key="error.button" /></button>
		 	</form>
		 	<br/>
			<br/>

		</div>
	</div>
</body>
	</html>
</fmt:bundle>