<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Delete user</title>
<meta charset="UTF-8">
</head>
<body>
	<div class="page">
		<div class="form">
			<p class="message">
				<fmt:message key="admin.delete.info" />
			</p>
			<form id="delete" method="POST" action="controller">
				<input type="hidden" name="command" value="delete_student"/>
				<select  class="custom-select" style="width:300px;" name ="studentId" >
					<option disabled><fmt:message key="admin.select.student"/></option>
					<c:forEach var="elem" items="${students}" varStatus="status">
					<option value="${elem.id}" ><c:out value="${elem}"/></option>
					</c:forEach>
				</select>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<p class="errormessage"><c:out value = "${errorStudentDeleteMessage}"/></p>
				<button type="submit" form="delete" class="button">
					<fmt:message key="admin.button.delete"/>
				</button>
				<p class="successmessage"><c:out value = "${studentDeleteMessage}"/></p>
			</form>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<form class="back-form" action="controller" method="POST">
				<input type="hidden" name="command" value="forward_page" />
				<button class="logBack" type="submit" name="forward"
					value="menu">
					<fmt:message key="back" />
				</button>
			</form>

			<form class="logout-form" action="controller" method="POST">
				<input type="hidden" name="command" value="logout" />
				<button class="logBack" type="submit">
					<fmt:message key="admin.add.logout" />
				</button>
			</form>
			
		</div>
	</div>
</body>
</html>
</fmt:bundle>