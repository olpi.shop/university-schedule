<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
<title>add new user</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
		<p class="message"> <fmt:message key="admin.addUser.info" /> </p>
		<form class="login-form" method="POST" action="controller">
		<input type="hidden" name="command" value="add_new_student" />
		
			<p class="message"><fmt:message key="admin.message.name"/>
			<input type="text" name="firstName" placeholder="<fmt:message key="admin.text.firtname"/>" pattern="((^[A-Z][a-z]{1,30}$)|(^[А-ЯЁ][а-я]{1,30}$))" maxlength="30"/>
					<span class="form__error"><fmt:message key="admin.valid.name"/></span> 
			<input type="text" name="lastName" placeholder="<fmt:message key="admin.text.lastname"/>"  pattern="((^[A-Z][a-z]{1,30}$)|(^[А-ЯЁ][а-я]{1,30}$))" maxlength="30"/>
					<span class="form__error"><fmt:message key="admin.valid.surname"/></span>
					
			<p class="message"><fmt:message key="admin.message.email"/>
			
			<input type="email" name="studentEmail" placeholder="<fmt:message key="admin.text.email"/>" pattern="^([\w\_\-\.]{2,40})@([\w\-\.]{2,40})(\.)([a-zA-Z]{2,5})$" maxlength="85"/>
			<span class="form__error"><fmt:message key="login.valid.email"/></span> 
			<input type="password" name="studentPassword" placeholder="<fmt:message key="admin.text.password"/>"  pattern="^[\w]{4,20}$" minlength="4" maxlength="20"/>
			<span class="form__error"><fmt:message key="login.valid.pass"/></span>
		
		
			<p class="message"><fmt:message key="admin.message.group"/>
			
			<select  style="width:200px;" name ="group" >
				<c:forEach var="elem" items="${groups}" varStatus="status">
				<option value="${elem.id}" ><c:out value="${elem}"/></option>
				</c:forEach>
			</select>
			<p class="message">
			<p class="successmessage"><c:out value = "${successAddNewStudent}"/></p>
			<button type="submit" class="button"> <fmt:message key="admin.addUser.button" /></button>
			<p class="errormessage"> ${failAddNewStudent} </p>
		 </form>
		
		<br/>
		
		<form  class="back-form" action="controller" method="POST">
		<input type="hidden" name="command" value="forward_page" />
		<button class="logBack" type="submit" name="forward" value="menu"><fmt:message key="back"/></button>
		</form>
		
		<form class="logout-form" action="controller" method="POST">
		<input type="hidden" name="command" value="logout" />
        <button class="logBack" type="submit"><fmt:message key="logout"/></button>
		</form>
       
		
		</div>
	</div>

</body>
</html>
</fmt:bundle>