<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/show_style.css" rel="stylesheet">
<title>Change password</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
		<p class="message"><fmt:message key="admin.showStudents" /></p>
		<br/>
		<table  align="center">
			<c:forEach var="elem" items="${studentsMap.get(page_number)}" varStatus="status">
				   <tr>
    					<td><p class="showmessage"><c:out value="${elem}"/></p></td>
   					</tr>				
			</c:forEach>
		</table>
		<br/>
		<c:if test="${page_number > 1 }">
		<form class="back-form" action="controller" method="POST">
			<input type="hidden" name="command" value="forward_page" />		
			<button class="preNext" type="submit" name="forward" value="show_all_students_back">
			<fmt:message key="past" />
			</button>
		</form>
		</c:if>
		<c:if test="${page_number < pages_count }">
		<form class="logout-form" action="controller" method="POST">
			<input type="hidden" name="command" value="forward_page" />		
			<button class="preNext" type="submit" name="forward" value="show_all_students_next">
			<fmt:message key="next" />
			</button>
		</form>			
		</c:if>
			<br/>
			<br/>
			<br/>			
			<form class="back-form" action="controller" method="POST">
				<input type="hidden" name="command" value="forward_page" />
				<button class="logBack" type="submit" name="forward"
					value="menu">
					<fmt:message key="back" />
				</button>
			</form>
			<form class="logout-form" action="controller" method="POST">
				<input type="hidden" name="command" value="logout" />
				<button class="logBack" type="submit">
					<fmt:message key="admin.add.logout" />
				</button>
			</form>	
		</div>
	</div>

</body>
	</html>
</fmt:bundle>