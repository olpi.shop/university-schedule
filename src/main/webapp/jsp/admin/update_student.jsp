<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Update user</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
			<p class="message">
				<fmt:message key="admin.update.info" />
			</p>
			<form id="show" method="POST" action="controller">
			<input type="hidden" name="command" value="find_student_for_update"  />
				<p class="message">
				<fmt:message key="admin.update.input" />
				<input type="text" name="studentId" pattern="^[0-9]{1,8}$" /> 
				<span class="form__error"> <fmt:message key="admin.valid.id"/> </span>	
				
				<p class="errormessage"><c:out value = "${failShowStudent}"/></p>
				<p class="message">
				<button type="submit" form="show" class="button" name="additionalCommand"
					value="show">
					<fmt:message key="admin.button.show"/>
				</button>
			</form>
					
			<c:if test="${ not empty student }">
			<form id="update" method="POST" action="controller">
				<input type="hidden" name="command" value="update_student" />
				
				<p class="message">ID: <c:out value="${student.id}"/></p>
            	<p class="message"><fmt:message key="admin.update.surname"/>: <c:out value="${student.lastName}"/> | 
            	<fmt:message key="admin.update.name"/>: <c:out value="${student.firstName}"/></p>
            	<p class="message"><fmt:message key="admin.text.email"/>: <c:out value="${studentEmail}"/> | 
            	<fmt:message key="admin.update.group"/>: <c:out value="${group}"/></p>
            		
            	<p class="message"><fmt:message key="admin.update.new.info" /><p>
					
				<input type="text" name="newFirstName" placeholder="<fmt:message key="admin.text.newfirtname"/>" pattern="((^[A-Z][a-z]{1,30}$)|(^[А-ЯЁ][а-я]{1,30}$))" maxlength="30"/>
				<span class="form__error"><fmt:message key="admin.valid.name"/></span> 
				<input type="text" name="newLastName" placeholder="<fmt:message key="admin.text.newlastname"/>"  pattern="((^[A-Z][a-z]{1,30}$)|(^[А-ЯЁ][а-я]{1,30}$))" maxlength="30"/>
				<span class="form__error"><fmt:message key="admin.valid.surname"/></span>
				<input type="email" name="newStudentEmail" placeholder="<fmt:message key="admin.text.newemail"/>" pattern="^([\w\_\-\.]{2,40})@([\w\-\.]{2,40})(\.)([a-zA-Z]{2,5})$" maxlength="85" />
				<span class="form__error"><fmt:message key="login.valid.email"/></span> 
					
				<p class="message"><fmt:message key="admin.message.newgroup"/>
				<select  class="custom-select" style="width:250px;" name ="newGroupId" >
					<option value="" selected><fmt:message key="admin.select.group"/></option>
					<c:forEach var="elem" items="${groups}" varStatus="status">
					<option value="${elem.id}" ><c:out value="${elem}"/></option>
					</c:forEach>
				</select>
				<br/>
				<p class="successmessage"> <c:out value = "${studentUpdateMessage}"/> </p>
				<button type="submit" form="update" class="button" name="additionalCommand"
					value="update">
					<fmt:message key="admin.button.update"/>
				</button>
				<p class="errormessage"><c:out value = "${invalidUpdateMessage}"/></p>
			</form>
        </c:if>
		
			<br/>
			<br/>
			<form class="back-form" action="controller" method="POST">
				<input type="hidden" name="command" value="forward_page" />
				<button class="logBack" type="submit" name="forward"
					value="menu">
					<fmt:message key="back" />
				</button>
			</form>

			<form class="logout-form" action="controller" method="POST">
				<input type="hidden" name="command" value="logout" />
				<button class="logBack" type="submit">
					<fmt:message key="admin.add.logout" />
				</button>
			</form>
			
		</div>
	</div>

</body>
	</html>
</fmt:bundle>