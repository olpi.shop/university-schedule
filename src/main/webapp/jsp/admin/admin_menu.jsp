<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Admin menu page</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
		<p class="message"> <fmt:message key="admin.hello" /> </p>
		<br /> 
		<form class="login-form" name="loginForm" method="POST" action="controller">
		<input type="hidden" name="command" value="forward_page" />
		
		<button class="btn" type="submit" name="forward" value="add_user"><fmt:message key="admin.add.student"/></button>
		<button class="btn" type="submit" name="forward" value="update_user"><fmt:message key="admin.update.student"/></button>
		<button class="btn" type="submit" name="forward" value="delete_user"><fmt:message key="admin.delete.student"/></button>
		<button class="btn" type="submit" name="forward" value="show_all_students"><fmt:message key="admin.showall.student"/></button>
		<button class="btnlast" type="submit" name="forward" value="change_pass"><fmt:message key="change.password"/></button>
        </form>
        
        <br/>
        <form class="logout-form" action="controller" method="POST">
		<input type="hidden" name="command" value="logout" />
        <button class="logBack" type="submit"><fmt:message key="logout"/></button>
		</form>
		</div>
	</div>
</body>
	</html>
</fmt:bundle>