<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/show_style.css"
	rel="stylesheet">
<title>Show schedule</title>
<meta charset="UTF-8">

<script type="text/javascript"
	src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</head>
<body>
	<div class="page">
		<div class="form">
			<p class="message">
				<fmt:message key="student.showSchedule" />
			</p>
			<br />
			<form id="show" method="POST" action="controller">
				<input type="hidden" name="command" value="show_schedule" />
				<p class="message">
					<fmt:message key="student.chooseDate" />
					:
				</p>
				<input class="calendar" type="text" name="period"
					value="01/09/2020-01/09/2020" /> <br /> <br />
				<button type="submit" form="show" class="button">
					<fmt:message key="admin.button.show" />
				</button>
			</form>
			<table align="center" border="1px" frame="void">
				<c:forEach var="dateSchedule"
					items="${dateScheduleMap.get(page_number)}" varStatus="status">
					<tr>
						<td><p class="message" align="left">
								<c:out value="${dateSchedule.toString(sessionScope.locale)}" />
								<c:forEach var="schedule" items="${dateSchedule.schedules}"
									varStatus="status">
									<p class="showmessage">
										<c:out value="${schedule}" />
								</c:forEach>
							</p></td>
					</tr>
				</c:forEach>
			</table>
			<br />
			<c:if test="${page_number > 1 }">
				<form class="back-form" action="controller" method="POST">
					<input type="hidden" name="command" value="show_schedule" />
					<button class="preNext" type="submit" name="forward"
						value="show_schedule_back">
						<fmt:message key="past" />
					</button>
				</form>
			</c:if>
			<c:if test="${page_number < pages_count }">
				<form class="logout-form" action="controller" method="POST">
					<input type="hidden" name="command" value="show_schedule" />
					<button class="preNext" type="submit" name="forward"
						value="show_schedule_next">
						<fmt:message key="next" />
					</button>
				</form>
			</c:if>
			<br /> <br /> <br />

			<form class="back-form" action="controller" method="POST">
				<input type="hidden" name="command" value="forward_page" />
				<button class="logBack" type="submit" name="forward" value="menu">
					<fmt:message key="back" />
				</button>
			</form>
			<form class="logout-form" action="controller" method="POST">
				<input type="hidden" name="command" value="logout" />
				<button class="logBack" type="submit">
					<fmt:message key="admin.add.logout" />
				</button>
			</form>
		</div>
	</div>
</body>
	</html>



	<script type="text/javascript">
		$(function() {

			$('input[name="period"]').daterangepicker({
				minDate : '09/01/2020',
				maxDate : '05/31/2021',
				autoUpdateInput : false,
				alwaysShowCalendars : true,
				locale : {
					cancelLabel : '<fmt:message key="student.close"/>',
					applyLabel : '<fmt:message key="student.select"/>'
				}
			});

			$('input[name="period"]').on(
					'apply.daterangepicker',
					function(ev, picker) {
						$(this).val(
								picker.startDate.format('DD/MM/YYYY') + '-'
										+ picker.endDate.format('DD/MM/YYYY'));
					});

		});
	</script>

</fmt:bundle>
