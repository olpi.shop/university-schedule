<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Student menu page</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
		<p class="message"> 
		<fmt:message key="student.hello" /> <c:out value="${student.firstName}"/> <c:out value="${student.lastName}"/>
		</p>
		<p class="message"> 
		<fmt:message key="student.welcome" />
		</p>
		<br/> 
		<form class="login-form" name="loginForm" method="POST" action="controller">
		<input type="hidden" name="command" value="forward_page" />
		<button class="btn" type="submit" name="forward" value="show_schedule"><fmt:message key="student.watch.button"/></button>
		<button class="btn" type="submit" name="forward" value="change_pass"><fmt:message key="change.password"/></button>
        </form>
        <br/>
         <br/>
        <br/>
        <form class="logout-form" action="controller" method="POST">
		<input type="hidden" name="command" value="logout" />
        <button class="logBack" type="submit"><fmt:message key="logout"/></button>
		</form>
		</div>
	</div>
</body>
	</html>
</fmt:bundle>