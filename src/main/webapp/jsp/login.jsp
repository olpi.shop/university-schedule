<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Login</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="login-page">
		<div class="form">
			<form class="login-form" name="loginForm" method="POST"
				action="controller">
				<input type="hidden" name="command" value="login" />
				<p class="message">
					<fmt:message key="login.welcome" />
					</p>
				<p class="message">
					<input type="email" name="email" placeholder="<fmt:message key="login.email"/>" pattern="^([\w\_\-\.]{2,40})@([\w\-\.]{2,40})(\.)([a-zA-Z]{2,5})$" maxlength="85"/> 
					<span class="form__error"><fmt:message key="login.valid.email"/></span> 
					<input type="password" name="password" placeholder="<fmt:message key="login.password"/>"  pattern="^[\w]{4,20}$" minlength="4" maxlength="20"/>
					<span class="form__error"><fmt:message key="login.valid.pass"/></span>
					<button type="submit" class="button">
						<fmt:message key="login.login" />
					</button>
					<br /> 
					<p class="errormessage"><c:out value = "${failLoginMessage}"/></p>
					<br />
			</form>
			<form action="controller" method="POST">
				<input type="hidden" name="command" value="locale" />
				<button class="lang" type="submit" name="locale" value="en_US">English</button>
				<button class="lang" type="submit" name="locale" value="ru_RU">Русский</button>
			</form>
		</div>
	</div>



</body>
	</html>
</fmt:bundle>