<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.locale}" />
<fmt:bundle basename="pagecontent">
	<!DOCTYPE html>
	<html>
<head>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<title>Change password</title>
<meta charset="UTF-8">

</head>
<body>
	<div class="page">
		<div class="form">
			<p class="message"><fmt:message key="admin.change.info" /></p>
			<form id="show" method="POST" action="controller">
			<input type="hidden" name="command" value="change_data"  />
			<br/>
			<p class="required-message">*<fmt:message key="admin.change.enterPreviousPass" /></p>
			<input type="password" required name="oldPassword" placeholder="<fmt:message key="admin.change.previousPass"/>" pattern="^[\w]{4,20}$" minlength="4" maxlength="20"/>
			<span class="form__error"><fmt:message key="login.valid.pass"/></span>
			<p class="errormessage"><c:out value = "${wrongOldPassword}"/></p>
			<br/>
			<p class="change-message"><fmt:message key="admin.change.enterNewEmail" /></p>
			<input type="email" name="newEmail" placeholder="<fmt:message key="admin.text.newemail"/>" pattern="^([\w\_\-\.]{2,40})@([\w\-\.]{2,40})(\.)([a-zA-Z]{2,5})$" maxlength="85" /> 
			<span class="form__error"><fmt:message key="login.valid.email"/></span> 		
			<br/>	
			<p class="required-message">*<fmt:message key="admin.change.newPassword" /></p>
			<input type="password" required name="newPassword" placeholder="<fmt:message key="admin.text.newPassword"/>" pattern="^[\w]{4,20}$" minlength="4" maxlength="20"/>
			<span class="form__error"><fmt:message key="login.valid.pass"/></span>
			<input type="password" required name="newPasswordRepeat" placeholder="<fmt:message key="admin.text.newPasswordAgain"/>" pattern="^[\w]{4,20}$" minlength="4" maxlength="20"/>
			<span class="form__error"><fmt:message key="login.valid.pass"/></span>	
			<p class="errormessage"><c:out value = "${failChangeData}"/></p>
			<br/>	
			<p class="successmessage"><c:out value = "${successChangeMessage}"/></p>		
			<button type="submit" form="show" class="button"><fmt:message key="admin.change.button"/>
			</button>
			</form>			
			<br/>
			<br/>
			<form class="back-form" action="controller" method="POST">
				<input type="hidden" name="command" value="forward_page" />
				<button class="logBack" type="submit" name="forward"
					value="menu">
					<fmt:message key="back" />
				</button>
			</form>
			<form class="logout-form" action="controller" method="POST">
				<input type="hidden" name="command" value="logout" />
				<button class="logBack" type="submit">
					<fmt:message key="admin.add.logout" />
				</button>
			</form>
			
		</div>
	</div>

</body>
	</html>
</fmt:bundle>