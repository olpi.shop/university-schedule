package com.olpi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;

@WebFilter(urlPatterns = { "/jsp/student/*" }, initParams = { @WebInitParam(name = "INDEX_PATH", value = "/index.jsp"),
		@WebInitParam(name = "STUDENT_PATH", value = "/jsp/student/student_menu.jsp") })
public class StudentPageRedirectSecurityFilter implements Filter {

	private String indexPath;
	private String studentPath;

	private static final String USER_ATTRIBUTE = "user";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		studentPath = filterConfig.getInitParameter("STUDENT_PATH");
		indexPath = filterConfig.getInitParameter("INDEX_PATH");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute(USER_ATTRIBUTE);

		if (user == null || !user.getRole().equals(UserRole.STUDENT)) {
			httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
		} else if (user.getRole().equals(UserRole.STUDENT)) {
			httpResponse.sendRedirect(httpRequest.getContextPath() + studentPath);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		indexPath = null;
		studentPath = null;
	}

}
