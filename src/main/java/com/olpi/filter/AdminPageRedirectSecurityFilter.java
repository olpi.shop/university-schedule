package com.olpi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;

@WebFilter(urlPatterns = { "/jsp/admin/*" }, initParams = { @WebInitParam(name = "INDEX_PATH", value = "/index.jsp"),
		@WebInitParam(name = "ADMIN_PATH", value = "/jsp/admin/admin_menu.jsp") })
public class AdminPageRedirectSecurityFilter implements Filter {

	private String indexPath;
	private String adminPath;

	private static final String USER_ATTRIBUTE = "user";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		adminPath = filterConfig.getInitParameter("ADMIN_PATH");
		indexPath = filterConfig.getInitParameter("INDEX_PATH");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute(USER_ATTRIBUTE);
		if (user == null || !user.getRole().equals(UserRole.ADMIN)) {
			httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
		} else if (user.getRole().equals(UserRole.ADMIN)) {
			httpResponse.sendRedirect(httpRequest.getContextPath() + adminPath);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		adminPath = null;
		indexPath = null;
	}

}
