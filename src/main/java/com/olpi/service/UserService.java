package com.olpi.service;

import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.dao.StudentDao;
import com.olpi.dao.TeacherDao;
import com.olpi.dao.TransactionManager;
import com.olpi.dao.UserDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.exception.RegistrationExeption;
import com.olpi.dao.jdbc.PgStudentDao;
import com.olpi.dao.jdbc.PgTeacherDao;
import com.olpi.dao.jdbc.PgUserDao;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.exceprion.ServiceException;

public class UserService {

	private static final Logger LOGGER = Logger.getLogger(UserService.class);

	private UserDao userDao = PgUserDao.getInstance();

	public Optional<User> authorize(String email, String password) throws ServiceException {
		String codedPassword = DigestUtils.md5Hex(password);
		Optional<User> user;
		try {
			user = userDao.authorize(email, codedPassword);
		} catch (DAOException e) {
			throw new ServiceException("Failed to authorizate user!", e);
		}
		LOGGER.log(Level.DEBUG, "User Authorization.");
		return user;
	}

	public User register(String email, Long id, UserRole role, String password) throws ServiceException {
		if (role.equals(UserRole.STUDENT) && !checkStudentId(id)) {
			throw new ServiceException("This student id does not exist!");
		} else if (role.equals(UserRole.TEACHER) && !checkTeacherId(id)) {
			throw new ServiceException("This teacher id does not exist!");
		}
		String codedPassword = DigestUtils.md5Hex(password);
		try {
			return userDao.register(email, id, role, codedPassword);
		} catch (RegistrationExeption ex) {
			throw new ServiceException("This email adress exist in DB");
		} catch (DAOException e) {
			throw new ServiceException("Failed to register user!", e);
		}
	}

	public User updateUser(User user, String newEmail, String newPassword) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "Update user.");
			String codedPassword = DigestUtils.md5Hex(newPassword);
			return userDao.updateUser(user, newEmail, codedPassword);
		} catch (DAOException e) {
			throw new ServiceException("Failed to update user!", e);
		}
	}

	public User updateUserEmail(User user, String newEmail) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "Update user email.");
			return userDao.updateUserEmail(user, newEmail);
		} catch (DAOException e) {
			throw new ServiceException("Failed to update user email!", e);
		}
	}

	public User updateUserPassword(User user, String newPassword) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "Update user email.");
			String codedPassword = DigestUtils.md5Hex(newPassword);
			return userDao.updateUserPassword(user, codedPassword);
		} catch (DAOException e) {
			throw new ServiceException("Failed to update user email!", e);
		}
	}

	public Optional<User> findUserByEmail(String email) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "find user by e-mail.");
			return userDao.findUserByEmail(email);
		} catch (DAOException e) {
			throw new ServiceException("Failed to find user by email!", e);
		}
	}

	public Optional<User> findUserById(Long id, UserRole role) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "find user by id and role.");
			return userDao.findUserById(id, role);
		} catch (DAOException e) {
			throw new ServiceException("Failed to authorizate user!", e);
		}
	}

	public void removeUser(String email) throws ServiceException {
		try {
			userDao.removeUser(email);
			LOGGER.log(Level.DEBUG, "Delete user.");
		} catch (DAOException e) {
			throw new ServiceException("Failed to remove user!", e);
		}

	}

	public void removeUser(Long id, UserRole role) throws ServiceException {
		try {
			userDao.removeUser(id, role);
			LOGGER.log(Level.DEBUG, "Delete user.");
		} catch (DAOException e) {
			throw new ServiceException("Failed to remove user!", e);
		}

	}

	public boolean checkUserEmail(String email) throws ServiceException {
		try {
			LOGGER.log(Level.DEBUG, "Check user email in base.");
			return userDao.findUserByEmail(email).isPresent();
		} catch (DAOException e) {
			throw new ServiceException("Failed to check user email in base", e);
		}
	}

	private boolean checkStudentId(Long id) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			manager.beginTransaction(studentDao);
			try {
				boolean isIdExist = studentDao.checkStudentId(id);
				LOGGER.log(Level.DEBUG, "Check student id.");
				return isIdExist;
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while checking student id!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to check student id!", ex);
		}
	}

	private boolean checkTeacherId(Long id) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			TeacherDao teacherDao = new PgTeacherDao();
			manager.beginTransaction(teacherDao);
			try {
				boolean isIdExist = teacherDao.checkTeacherId(id);
				manager.endTransaction();
				LOGGER.log(Level.DEBUG, "Check teacher id.");
				return isIdExist;
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while checking teacher id!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to check teacher id!", ex);
		}

	}

}
