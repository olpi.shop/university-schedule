package com.olpi.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.dao.*;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.exception.RegistrationExeption;
import com.olpi.domain.*;
import com.olpi.domain.enums.AcademicHour;
import com.olpi.domain.enums.Classroom;
import com.olpi.domain.enums.Department;
import com.olpi.domain.enums.UserRole;

/*
 * This class is not related to business logic, 
 * it is optional. 
 * It generates data to demonstrate the operation of the program.
 * His methods are used in the class UniversityScheduleApp, in method main.
 */

public class DataGenerator {

	private static final Logger LOGGER = Logger.getLogger(DataGenerator.class);

	private WeekDayDao weekDayDao;
	private GroupDao groupDao;
	private LessonDao lessonDao;
	private DateScheduleDao dateScheduleDao;
	private ScheduleDao scheduleDao;
	private StudentDao studentDao;
	private SubjectDao subjectDao;
	private TeacherDao teacherDao;
	private UserDao userDao;

	private List<Classroom> rooms;
	private List<AcademicHour> hours;
	private Random random;

	public static class Builder {
		private WeekDayDao weekDayDao;
		private GroupDao groupDao;
		private LessonDao lessonDao;
		private DateScheduleDao dateScheduleDao;
		private ScheduleDao scheduleDao;
		private StudentDao studentDao;
		private SubjectDao subjectDao;
		private TeacherDao teacherDao;
		private UserDao userDao;

		public Builder weekDayDao(WeekDayDao weekDayDao) {
			this.weekDayDao = weekDayDao;
			return this;
		}

		public Builder groupDao(GroupDao groupDao) {
			this.groupDao = groupDao;
			return this;
		}

		public Builder lessonDao(LessonDao lessonDao) {
			this.lessonDao = lessonDao;
			return this;
		}

		public Builder dateScheduleDao(DateScheduleDao dateScheduleDao) {
			this.dateScheduleDao = dateScheduleDao;
			return this;
		}

		public Builder studentDao(StudentDao studentDao) {
			this.studentDao = studentDao;
			return this;
		}

		public Builder scheduleDao(ScheduleDao scheduleDao) {
			this.scheduleDao = scheduleDao;
			return this;
		}

		public Builder subjectDao(SubjectDao subjectDao) {
			this.subjectDao = subjectDao;
			return this;
		}

		public Builder teacherDao(TeacherDao teacherDao) {
			this.teacherDao = teacherDao;
			return this;
		}

		public Builder userDao(UserDao userDao) {
			this.userDao = userDao;
			return this;
		}

		public DataGenerator build() {
			return new DataGenerator(this);
		}
	}

	private DataGenerator(Builder builder) {

		this.weekDayDao = builder.weekDayDao;
		this.groupDao = builder.groupDao;
		this.lessonDao = builder.lessonDao;
		this.dateScheduleDao = builder.dateScheduleDao;
		this.scheduleDao = builder.scheduleDao;
		this.studentDao = builder.studentDao;
		this.subjectDao = builder.subjectDao;
		this.teacherDao = builder.teacherDao;
		this.userDao = builder.userDao;
		random = new Random();
		rooms = new ArrayList<>();
		classInit();
		hours = new ArrayList<>();
		hourInit();
	}

	public void generateGroups(int count) throws DAOException {
		System.out.println("Generate groups and students");
		for (int i = 0; i < count; i++) {
			Group group = groupDao.createOrUpdate(new Group.Builder().name(generateGroupName())
					.stream(generateCourseOrStream()).course(generateCourseOrStream()).build());

			generateStudents(group.getId(), (random.nextInt(20 + 1 - 15) + 15));
		}
	}

	public void generateTeachers(int count) throws DAOException {
		System.out.println("Generate teachers");
		for (int i = 0; i < count; i++) {
			Teacher teacher = new Teacher.Builder().firstName(generateFirstName()).lastName(generateLastName())
					.deparment(generateDepartment()).build();
			teacherDao.createOrUpdate(teacher);

		}
	}

	public void generateSubjects() throws DAOException {

		System.out.println("Generate subjects");
		ArrayList<Subject> subjects = new ArrayList<>();
		subjects.add(new Subject.Builder().name("Biology").deparment(Department.ECOLOGY).build());
		subjects.add(new Subject.Builder().name("Geography").deparment(Department.ECOLOGY).build());
		subjects.add(new Subject.Builder().name("Zoology").deparment(Department.ECOLOGY).build());

		subjects.add(new Subject.Builder().name("Economic theory").deparment(Department.ECONOMICS).build());
		subjects.add(new Subject.Builder().name("Economics").deparment(Department.ECONOMICS).build());
		subjects.add(new Subject.Builder().name("Marketing").deparment(Department.ECONOMICS).build());

		subjects.add(new Subject.Builder().name("Java").deparment(Department.INFORMATION_SYSTEMS).build());
		subjects.add(new Subject.Builder().name("C++").deparment(Department.INFORMATION_SYSTEMS).build());
		subjects.add(new Subject.Builder().name("Web design").deparment(Department.INFORMATION_SYSTEMS).build());

		subjects.add(new Subject.Builder().name("Physics").deparment(Department.MATH).build());
		subjects.add(new Subject.Builder().name("Chemistry").deparment(Department.MATH).build());
		subjects.add(new Subject.Builder().name("Math").deparment(Department.MATH).build());

		subjects.add(new Subject.Builder().name("Journalism").deparment(Department.PHILOSOPHY).build());
		subjects.add(new Subject.Builder().name("Sociology").deparment(Department.PHILOSOPHY).build());
		subjects.add(new Subject.Builder().name("Psychology").deparment(Department.PHILOSOPHY).build());

		for (int i = 0; i < subjects.size(); i++) {
			Long subjectId = subjectDao.createOrUpdate(subjects.get(i)).getId();
			List<Teacher> teachers = new ArrayList<>();
			teachers.add(generateTeacherToSubject());
			teachers.add(generateTeacherToSubject());
			subjectDao.addTeachersToSubject(teachers, subjectId);
		}
	}

	public void generateLesson(int min, int max) throws DAOException {
		lessonDao.removeAll();
		System.out.println("Generate lessons");
		List<Group> groups = groupDao.findAll();
		Subject subject;
		Teacher teacher;
		List<Subject> subjects = subjectDao.findAll();

		for (int i = 0; i < groups.size(); i++) {
			int subjectCount = random.nextInt(max + 1 - min) + min;
			for (int j = 0; j < subjectCount; j++) {
				subject = subjects.get(random.nextInt(subjects.size()));
				teacher = generateTeacherForLesson(subject);
				Lesson lesson = new Lesson.Builder().group(groups.get(i)).subject(subject).teacher(teacher).build();
				lessonDao.createOrUpdate(lesson);
			}
		}
	}

	public void generateSchedule(int min, int max) throws DAOException {
		scheduleDao.removeAll();
		System.out.println("Generate schedule");
		List<Lesson> lessons = lessonDao.findAll();
		for (int i = 0; i < lessons.size(); i++) {
			int lessonCount = random.nextInt(max + 1 - min) + min;
			for (int j = 0; j < lessonCount; j++) {
				Schedule schedule = new Schedule.Builder().lesson(lessons.get(i)).hour(generateHourToSchedule())
						.room(generateRoomToSchedule()).build();
				scheduleDao.createOrUpdate(schedule);
			}
		}
	}

	public void generateWeekDaySchedule() throws DAOException {
		System.out.println("Generate week schedule");
		weekDayDao.removeAll();

		List<Group> allGroups = groupDao.findAll();

		for (int i = 0; i < allGroups.size(); i++) {
			List<Schedule> groupSchedule = scheduleDao.findByGroupId(allGroups.get(i).getId());
			int dayIndex = 1;
			int countInDay = groupSchedule.size() / 5;
			int addedCount = 0;
			int balance = groupSchedule.size() - (countInDay * 5);
			if (balance != 0) {
				countInDay++;
			}
			List<AcademicHour> addedHours = new ArrayList<>();
			List<Schedule> dayShedule = new ArrayList<>();
			for (int j = 0; j < groupSchedule.size(); j++) {

				Schedule schedule = groupSchedule.get(random.nextInt(groupSchedule.size()));
				AcademicHour hour = schedule.getHour();

				while (addedHours.contains(hour)) {
					schedule = groupSchedule.get(random.nextInt(groupSchedule.size()));
					hour = schedule.getHour();
				}
				addedHours.add(hour);
				dayShedule.add(schedule);
				addedCount++;

				if (addedCount == countInDay || j == groupSchedule.size() - 1) {
					dayShedule.sort((s1, s2) -> s1.getHour().getTime().compareTo(s2.getHour().getTime()));
					weekDayDao.addScheduleInDay(DayOfWeek.of(dayIndex), dayShedule);
					dayIndex++;
					addedCount = 0;
					dayShedule.clear();
					addedHours.clear();
					balance--;
					if (balance == 0) {
						countInDay--;
					}
				}
			}
		}
	}

	public void generateDateSchedule(LocalDate startDate, LocalDate endDate) throws DAOException {

		System.out.println("Generate month schedule");
		dateScheduleDao.removeAll();
		while (!startDate.equals(endDate)) {

			WeekDaySchedule weekDaySchedule = weekDayDao.findById(startDate.getDayOfWeek())
					.orElse(new WeekDaySchedule(startDate.getDayOfWeek()));

			DateSchedule dateSchedule = new DateSchedule(startDate);
			dateSchedule.setSchedules(weekDaySchedule.getSchedule());
			dateScheduleDao.createOrUpdate(dateSchedule);
			startDate = startDate.plusDays(1);
		}
	}

	private void generateStudents(Long groupId, int count) throws DAOException {
		for (int i = 0; i < count; i++) {
			Student student = new Student.Builder().firstName(generateFirstName()).lastName(generateLastName())
					.groupId(groupId).build();
			student = studentDao.createOrUpdate(student);
			try {
				userDao.register(generateEmail(), student.getId(), UserRole.STUDENT, DigestUtils.md5Hex("1111"));
			} catch (RegistrationExeption e) {
				LOGGER.log(Level.DEBUG, "This e-mail already exist in DB!.", e);
			}
		}
	}

	private String generateEmail() {

		StringJoiner joiner = new StringJoiner("");
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add("@");
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add(".");
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'a')));
		return joiner.toString();
	}

	private String generateGroupName() {

		StringJoiner joiner = new StringJoiner("");
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'A')));
		joiner.add(String.valueOf((char) (random.nextInt(26) + 'A')));
		return joiner.toString();
	}

	private String generateFirstName() {

		String[] firstNames = { "Liam", "Emma", "Noah", "Olivia", "Mason", "Ava", "Ethan", "Sophia", "Logan",
				"Isabella", "Lucas", "Mia", "Jackson", "Charlotte", "Aiden", "Amelia", "Oliver", "Emily", "Jacob",
				"Madison" };

		return firstNames[random.nextInt(firstNames.length)];
	}

	private String generateLastName() {

		String[] lastNames = { "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore",
				"Taylor", "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Wood", "Lewis",
				"Wild" };

		return lastNames[random.nextInt(lastNames.length)];
	}

	private int generateCourseOrStream() {
		return random.nextInt(6 - 1) + 1;
	}

	private Department generateDepartment() {

		Department[] departments = { Department.ECOLOGY, Department.ECONOMICS, Department.INFORMATION_SYSTEMS,
				Department.MATH, Department.PHILOSOPHY };

		return departments[random.nextInt(departments.length)];
	}

	private AcademicHour generateHourToSchedule() {
		return hours.get(random.nextInt(hours.size()));
	}

	private Classroom generateRoomToSchedule() {
		return rooms.get(random.nextInt(rooms.size()));
	}

	private Teacher generateTeacherToSubject() throws DAOException {

		List<Teacher> teachers = teacherDao.findAll();
		return teachers.get(random.nextInt(teachers.size()));

	}

	private Teacher generateTeacherForLesson(Subject subject) throws DAOException {

		List<Teacher> teachers = subjectDao.findTeachersBySubjectId(subject.getId());
		return teachers.get(random.nextInt(teachers.size()));

	}

	private void hourInit() {
		hours.add(AcademicHour.FIRST);
		hours.add(AcademicHour.SECOND);
		hours.add(AcademicHour.THIRD);
		hours.add(AcademicHour.FOURTH);
		hours.add(AcademicHour.FIVETH);
		hours.add(AcademicHour.SEVENTH);
		hours.add(AcademicHour.EIGHTH);
		hours.add(AcademicHour.NINETH);
	}

	private void classInit() {
		rooms.add(Classroom.A101);
		rooms.add(Classroom.A102);
		rooms.add(Classroom.A103);
		rooms.add(Classroom.A104);
		rooms.add(Classroom.A201);
		rooms.add(Classroom.A202);
		rooms.add(Classroom.A203);
		rooms.add(Classroom.A204);
		rooms.add(Classroom.A301);
		rooms.add(Classroom.A302);
		rooms.add(Classroom.A303);
		rooms.add(Classroom.A304);
		rooms.add(Classroom.B101);
		rooms.add(Classroom.B102);
		rooms.add(Classroom.B103);
		rooms.add(Classroom.B104);
		rooms.add(Classroom.B201);
		rooms.add(Classroom.B202);
		rooms.add(Classroom.B203);
		rooms.add(Classroom.B204);
		rooms.add(Classroom.B301);
		rooms.add(Classroom.B302);
		rooms.add(Classroom.B303);
		rooms.add(Classroom.B304);
		rooms.add(Classroom.C101);
		rooms.add(Classroom.C102);
		rooms.add(Classroom.C103);
		rooms.add(Classroom.C104);
		rooms.add(Classroom.C201);
		rooms.add(Classroom.C202);
		rooms.add(Classroom.C203);
		rooms.add(Classroom.C204);
		rooms.add(Classroom.C301);
		rooms.add(Classroom.C302);
		rooms.add(Classroom.C303);
		rooms.add(Classroom.C304);
		rooms.add(Classroom.D101);
		rooms.add(Classroom.D102);
		rooms.add(Classroom.D103);
		rooms.add(Classroom.D104);
		rooms.add(Classroom.D201);
		rooms.add(Classroom.D202);
		rooms.add(Classroom.D203);
		rooms.add(Classroom.D204);
		rooms.add(Classroom.D301);
		rooms.add(Classroom.D302);
		rooms.add(Classroom.D303);
		rooms.add(Classroom.D304);
	}

}
