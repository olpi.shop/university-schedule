package com.olpi.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.dao.GroupDao;
import com.olpi.dao.TransactionManager;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.jdbc.PgGroupDao;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.domain.Group;
import com.olpi.service.exceprion.ServiceException;

public class GroupService {

	private static final Logger LOGGER = Logger.getLogger(GroupService.class);

	public Optional<Group> findGroupById(Long groupId) throws ServiceException {
		if (groupId < 0) {
			throw new ServiceException("Wrong id!");
		}
		try {
			TransactionManager manager = new TransactionManager();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(groupDao);
			try {
				LOGGER.log(Level.DEBUG, "Find all groups without students.");
				return groupDao.findGroupWithoutStudents(groupId);
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while find all groups!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find all groups!", ex);
		}
	}

	public List<Group> findAllGroupsWithoutStudents() throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(groupDao);
			try {
				List<Group> groups = groupDao.findAllGroupsWithoutStudents();
				LOGGER.log(Level.DEBUG, "Find all groups without students.");
				return groups;
			} finally {
				manager.endTransaction();
			}
		} catch (DAOException e) {
			throw new ServiceException("Failed access to db while find all groups withot students!", e);
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find all groups!", ex);
		}
	}

	public List<Group> findAllGroups() throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(groupDao);
			try {
				List<Group> groups = groupDao.findAll();
				LOGGER.log(Level.DEBUG, "Find all groups with students.");
				return groups;
			} finally {
				manager.endTransaction();
			}
		} catch (DAOException e) {
			throw new ServiceException("Failed access to db while find all groups with students!", e);
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find all groups!", ex);
		}
	}

}
