package com.olpi.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.dao.DateScheduleDao;
import com.olpi.dao.GroupDao;
import com.olpi.dao.TransactionManager;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.jdbc.PgDateScheduleDao;
import com.olpi.dao.jdbc.PgGroupDao;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.domain.DateSchedule;
import com.olpi.service.exceprion.ServiceException;

public class DateScheduleService {

	private static final Logger LOGGER = Logger.getLogger(DateScheduleService.class);

	public DateSchedule findScheduleForGroupByDate(LocalDate dateId, Long groupId) throws ServiceException {
		if (groupId < 0) {
			throw new ServiceException("Wrong data!");
		}
		try {
			TransactionManager manager = new TransactionManager();
			DateScheduleDao dateDao = new PgDateScheduleDao();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(dateDao, groupDao);
			try {
				if (groupDao.checkGroupId(groupId)) {
					LOGGER.log(Level.DEBUG, "Find schedule by groupId and date.");
					return dateDao.findScheduleByGroupId(dateId, groupId);
				} else {
					throw new ServiceException("This group id does not exist in DB!");
				}
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while finding schedule for group by date!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find schedule for group by date!", ex);
		}
	}

	public List<DateSchedule> findScheduleForGroupByPeriod(List<LocalDate> dates, Long groupId)
			throws ServiceException {
		if (groupId < 0) {
			throw new ServiceException("Wrong data!");
		}
		List<DateSchedule> schedules = new ArrayList<>();
		try {
			TransactionManager manager = new TransactionManager();
			DateScheduleDao dateDao = new PgDateScheduleDao();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(dateDao, groupDao);
			try {
				if (groupDao.checkGroupId(groupId)) {
					for (LocalDate date : dates) {
						schedules.add(dateDao.findScheduleByGroupId(date, groupId));
					}
					manager.endTransaction();
					LOGGER.log(Level.DEBUG, "Find schedules by groupId and period of time.");
					return schedules;
				} else {
					manager.endTransaction();
					throw new ServiceException("This group id does not exist in DB!");
				}
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while finding schedule for group by period of time!",
						e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find schedule for group by period of time!", ex);
		}
	}

}
