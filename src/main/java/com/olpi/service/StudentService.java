package com.olpi.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.dao.GroupDao;
import com.olpi.dao.StudentDao;
import com.olpi.dao.TransactionManager;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.jdbc.PgGroupDao;
import com.olpi.dao.jdbc.PgStudentDao;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.domain.Student;
import com.olpi.service.exceprion.ServiceException;

public class StudentService {

	private static final Logger LOGGER = Logger.getLogger(StudentService.class);

	public Student create(String firstName, String lastName, Long groupId) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			GroupDao groupDao = new PgGroupDao();
			manager.beginTransaction(groupDao, studentDao);
			try {
				if (groupDao.checkGroupId(groupId)) {
					Student student = new Student.Builder().firstName(firstName).lastName(lastName).groupId(groupId)
							.build();
					student = studentDao.createOrUpdate(student);
					LOGGER.log(Level.DEBUG, "Create new student.");
					return student;
				} else {
					throw new ServiceException("This group id does not exist in DB!");
				}
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while create student!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find schedule for group by date!!", ex);
		}
	}

	public Student update(Student student) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			manager.beginTransaction(studentDao);
			try {
				if (studentDao.checkStudentId(student.getId())) {
					student = studentDao.createOrUpdate(student);
					LOGGER.log(Level.DEBUG, "Update student.");
					return student;
				} else {
					throw new ServiceException("This student has id, but this Id does not exist in DB!");
				}
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while update student!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to update student!", ex);
		}
	}

	public Optional<Student> findStudentById(Long studentId) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			manager.beginTransaction(studentDao);
			try {
				Optional<Student> student = studentDao.findById(studentId);
				LOGGER.log(Level.DEBUG, "Find student by id.");
				return student;
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while finding student!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find student by id", ex);
		}
	}

	public List<Student> findAllStudents() throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			manager.beginTransaction(studentDao);
			try {
				List<Student> students = studentDao.findAll();
				LOGGER.log(Level.DEBUG, "Find all students");
				return students;
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while finding all students!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to find all students!", ex);
		}
	}

	public void removeStudent(Long studentId) throws ServiceException {
		try {
			TransactionManager manager = new TransactionManager();
			StudentDao studentDao = new PgStudentDao();
			manager.beginTransaction(studentDao);
			try {
				studentDao.remove(studentId);
				LOGGER.log(Level.DEBUG, "Remove student.");
			} catch (DAOException e) {
				throw new ServiceException("Failed access to db while removing student!", e);
			} finally {
				manager.endTransaction();
			}
		} catch (PoolException ex) {
			throw new ServiceException("Failed to remove student id!", ex);
		}
	}

}
