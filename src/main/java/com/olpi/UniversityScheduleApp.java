package com.olpi;

import java.time.LocalDate;

import com.olpi.dao.*;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.jdbc.*;
import com.olpi.dao.pool.ConnectionPool;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.service.DataGenerator;

public class UniversityScheduleApp {
	public static void main(String[] args) throws DAOException, PoolException {

		try {
			ConnectionPool.getInstance();

			StudentDao studentDao = new PgStudentDao();
			StudentDao studentDao1 = new PgStudentDao();
			TeacherDao teacherDao = new PgTeacherDao();
			GroupDao groupDao = new PgGroupDao();
			SubjectDao subjectDao = new PgSubjectDao();
			LessonDao lessonDao = new PgLessonDao();
			ScheduleDao scheduleDao = new PgScheduleDao();
			WeekDayDao weekDayDao = new PgWeekDayDao();
			DateScheduleDao dateScheduleDao = new PgDateScheduleDao();

			TransactionManager manager = new TransactionManager();
			manager.beginTransaction(studentDao, studentDao1, teacherDao, groupDao, subjectDao, lessonDao, scheduleDao, weekDayDao,
					dateScheduleDao);
			UserDao userDao = PgUserDao.getInstance();
			DataGenerator generator = new DataGenerator.Builder().weekDayDao(weekDayDao).groupDao(groupDao)
					.lessonDao(lessonDao).dateScheduleDao(dateScheduleDao).scheduleDao(scheduleDao)
					.studentDao(studentDao).subjectDao(subjectDao).teacherDao(teacherDao).userDao(userDao).build();

			generator.generateGroups(10);
			generator.generateTeachers(10);
			generator.generateSubjects();
			generator.generateLesson(5, 7);
			generator.generateSchedule(2, 4);
			generator.generateWeekDaySchedule();
			generator.generateDateSchedule(LocalDate.of(2020, 9, 1), LocalDate.of(2021, 5, 31));

			manager.endTransaction();

			System.out.println("Finish!");

		} finally {
			ConnectionPool.getInstance().destroy();
		}
	}
}
