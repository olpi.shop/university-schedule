package com.olpi.controller;

public class Router {
	public enum RouterType {
		FORWARD, REDIRECT;
	}

	private String page;
	private RouterType type = RouterType.FORWARD;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public RouterType getType() {
		return type;
	}

	public void setType(RouterType type) {
		this.type = type;
	}
}
