package com.olpi.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.CommandFactory;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.MessageManager;
import com.olpi.controller.Router.RouterType;
import com.olpi.dao.pool.ConnectionPool;
import com.olpi.dao.pool.exception.PoolException;

@WebServlet(name = "controller", urlPatterns = { "/controller" }, loadOnStartup = 1)
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(Controller.class);

	public static final String LOCALE_ATTRIBUTE = "locale";
	private static final String VALIABLE_ERROR_MESSAGE = "errorMessage";

	public Controller() {
		super();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			ConnectionPool.getInstance();
		} catch (PoolException e) {
			LOGGER.log(Level.ERROR, "Cannot init pool!", e);
		}
		LOGGER.log(Level.DEBUG, "Controller has been initialized");
	}

	@Override
	public void destroy() {
		try {
			ConnectionPool.getInstance().destroy();
		} catch (PoolException e) {
			LOGGER.log(Level.ERROR, "Cannot destroy pool!");
		}
		LOGGER.log(Level.DEBUG, "Controller has been destroyed");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(404);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);

		String nameCommand = request.getParameter("command");
		if (nameCommand == null) {
			LOGGER.log(Level.ERROR, "Command is empty!");
			request.setAttribute(VALIABLE_ERROR_MESSAGE, message.getMessage("error.commandnull"));
			response.sendError(500);
		} else {
			Command command = CommandFactory.defineCommand(nameCommand);
			if (command == null) {
				LOGGER.log(Level.ERROR, "Illigal command");
				request.setAttribute(VALIABLE_ERROR_MESSAGE, message.getMessage("error.illigalCommand"));
				response.sendError(500);
			} else {
				try {
					Router router = command.execute(request, response);
					if (router.getType().equals(RouterType.FORWARD)) {
						request.getRequestDispatcher(router.getPage()).forward(request, response);
					} else {
						String page = router.getPage();
						response.sendRedirect(request.getContextPath() + page);
					}
				} catch (CommandException e) {
					LOGGER.error("Cannot execute command." + e.getMessage());
					request.setAttribute(VALIABLE_ERROR_MESSAGE, e.getMessage());
					response.sendError(500);
				}
			}
		}
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
