package com.olpi.command;

import com.olpi.command.impl.*;
import com.olpi.service.*;

public enum CommandType {
	LOGIN(new LoginCommand(new UserService(), new StudentService())), 
	LOCALE(new LocalCommand()),
	FORWARD_PAGE(new ForwardPageCommand(new StudentService(), new GroupService())), 
	LOGOUT(new LogoutCommand()),
	FIND_STUDENT_FOR_UPDATE(new FindStudentForUpdateCommand(new UserService(), new StudentService(), new GroupService())),
	DELETE_STUDENT(new DeleteStudentCommand(new UserService(), new StudentService())),
	UPDATE_STUDENT(new UpdateStudentCommand(new StudentService(), new UserService(), new GroupService())),
	CHANGE_DATA(new ChangeDataCommand(new UserService())),
	SHOW_SCHEDULE(new ShowScheduleCommand(new DateScheduleService())),
	ADD_NEW_STUDENT(new AddStudentCommand(new UserService(), new StudentService()));

	private Command command;

	CommandType(Command command) {
		this.command = command;
	}

	public Command getCommand() {
		return command;
	}
}
