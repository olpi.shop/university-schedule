package com.olpi.command.util;

public class Validator {
	private static final String EMAIL_REGEX = "^([\\w\\-\\.]{1,40})@([\\w\\-\\.]{1,40})(\\.)([a-zA-Z]{1,5})$";
	private static final String PASSWORD_REGEX = "^[\\w]{4,20}$";
	private static final String NAME_REGEX = "((^[A-Z][a-z]{1,30}$)|(^[А-ЯЁ][а-я]{1,30}$))";

	private Validator() {
		throw new IllegalStateException("Utility class");
	}

	public static boolean isNotNullOrEmpty(String text) {
		return text != null && !text.isEmpty();
	}

	public static boolean isEmailValid(String email) {
		return email != null && !email.isEmpty() && email.matches(EMAIL_REGEX);
	}

	public static boolean isPasswordValid(String password) {
		return password != null && !password.isEmpty() && password.matches(PASSWORD_REGEX);
	}

	public static boolean isNameValid(String name) {
		return name != null && !name.isEmpty() && name.matches(NAME_REGEX);
	}

	public static boolean isLong(String text) {
		try {
			Long.parseLong(text);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

}
