package com.olpi.command.util;

public class PathManager {

	private PathManager() {
		throw new IllegalStateException("Utility class");
	}

	public static final String JSP_ERROR404 = "/jsp/errors/error404.jsp";
	public static final String JSP_ERROR500 = "/jsp/errors/error500.jsp";
	public static final String JSP_INDEX = "/index.jsp";
	public static final String JSP_LOGIN = "/jsp/login.jsp";
	public static final String JSP_CHANGE_PASS = "/jsp/common/change_pass.jsp";
	public static final String JSP_ADMIN_MENU = "/jsp/admin/admin_menu.jsp";
	public static final String JSP_ADMIN_ADD_STUDENT = "/jsp/admin/add_student.jsp";
	public static final String JSP_ADMIN_UPDATE_STUDENT = "/jsp/admin/update_student.jsp";
	public static final String JSP_ADMIN_DELETE_STUDENT = "/jsp/admin/delete_student.jsp";
	public static final String JSP_ADMIN_SHOW_ALL_STUDENTS = "/jsp/admin/show_all_students.jsp";
	public static final String JSP_STUDENT_MENU = "/jsp/student/student_menu.jsp";
	public static final String JSP_STUDENT_SHOW_SCHEDULE = "/jsp/student/student_schedule.jsp";
	
}
