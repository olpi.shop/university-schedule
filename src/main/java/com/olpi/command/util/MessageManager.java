package com.olpi.command.util;

import java.util.ResourceBundle;
import java.util.Locale;

public enum MessageManager {

	EN_US(ResourceBundle.getBundle("message", new Locale("en", "US"))),
	RU_RU(ResourceBundle.getBundle("message", new Locale("ru", "RU")));

	private ResourceBundle boundle;

	private MessageManager(ResourceBundle boundle) {
		this.boundle = boundle;
	}

	public String getMessage(String key) {
		return boundle.getString(key);
	}
}
