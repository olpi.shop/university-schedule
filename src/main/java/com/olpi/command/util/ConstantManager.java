package com.olpi.command.util;

public class ConstantManager {

	private ConstantManager() {
		throw new IllegalStateException("Utility class");
	}

	public static final String USER_ATTRIBUTE = "user";
	public static final String LOCALE_ATTRIBUTE = "locale";
	public static final String STUDENT_ID_ATTRIBUTE = "studentId";
	public static final String STUDENT_EMAIL_ATTRIBUTE = "studentEmail";
	public static final String STUDENT_ATTRIBUTE = "student";
	public static final String STUDENTS_ATTRIBUTE = "students";
	public static final String GROUP_ATTRIBUTE = "group";
	public static final String GROUPS_ATTRIBUTE = "groups";
	public static final String PAGES_COUNT_ATTRIBUTE = "pages_count";
	public static final String PAGE_NUMBER_ATTRIBUTE = "page_number";
	public static final String STUDENT_MAP_ATTRIBUTE = "studentsMap";
	public static final String DAY_SCHEDULE_MAP_ATTRIBUTE = "dateScheduleMap";

	public static final String FORWARD_PARAMETER = "forward";
	public static final String OLD_PASSWORD_PARAMETER = "oldPassword";
	public static final String NEW_EMAIL_PARAMETER = "newEmail";
	public static final String NEW_PASSWORD_PARAMETER = "newPassword";
	public static final String NEW_PASSWORD_REPEAT_PARAMETER = "newPasswordRepeat";
	public static final String PERIOD_PARAMETER = "period";

}
