package com.olpi.command;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class CommandFactory {
	private static final Logger LOGGER = Logger.getLogger(CommandFactory.class);

	private CommandFactory() {
		throw new IllegalStateException("Utility class");
	}

	public static Command defineCommand(String nameCommand) {
		if (nameCommand != null && !nameCommand.isEmpty()) {
			return CommandType.valueOf(nameCommand.toUpperCase()).getCommand();
		}
		LOGGER.log(Level.ERROR, "There is unsupported command!");
		return null;
	}

}
