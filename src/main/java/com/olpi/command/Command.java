package com.olpi.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.olpi.command.exception.CommandException;
import com.olpi.controller.Router;

public interface Command {

	Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException;

}
