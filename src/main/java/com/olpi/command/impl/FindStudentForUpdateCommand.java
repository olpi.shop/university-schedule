package com.olpi.command.impl;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.command.util.Validator;
import com.olpi.controller.Router;
import com.olpi.domain.Group;
import com.olpi.domain.Student;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.GroupService;
import com.olpi.service.StudentService;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class FindStudentForUpdateCommand implements Command {

	private static final Logger LOGGER = Logger.getLogger(FindStudentForUpdateCommand.class);

	private StudentService studentService;
	private UserService userService;
	private GroupService groupService;

	private static final String VARIABLE_FAIL_SHOW_STUDENT = "failShowStudent";

	public FindStudentForUpdateCommand(UserService userService, StudentService studentService,
			GroupService groupService) {
		this.studentService = studentService;
		this.userService = userService;
		this.groupService = groupService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);
		Router router = new Router();
		router.setPage(PathManager.JSP_ADMIN_UPDATE_STUDENT);

		String id = request.getParameter(ConstantManager.STUDENT_ID_ATTRIBUTE);
		if (id != null && Validator.isLong(id)) {
			Long studentId = Long.valueOf(request.getParameter(ConstantManager.STUDENT_ID_ATTRIBUTE));
			try {
				User user;
				Optional<User> optionalUser = userService.findUserById(studentId, UserRole.STUDENT);
				if (optionalUser.isPresent()) {
					user = optionalUser.get();
				} else {
					LOGGER.log(Level.DEBUG, "Student ID not found!");
					request.setAttribute(VARIABLE_FAIL_SHOW_STUDENT, message.getMessage("failed.idNotFound"));
					return router;
				}
				Student student;
				Optional<Student> optionalStudent = studentService.findStudentById(studentId);
				if (optionalStudent.isPresent()) {
					student = optionalStudent.get();
				} else {
					LOGGER.log(Level.DEBUG, "Id exists in User base, but not exists in Student base!");
					request.setAttribute(VARIABLE_FAIL_SHOW_STUDENT, message.getMessage("failed.studentIdNotFound"));
					return router;
				}
				Group group;
				Optional<Group> optionalGroup = groupService.findGroupById(student.getGroupId());
				if (optionalGroup.isPresent()) {
					group = optionalGroup.get();
				} else {
					LOGGER.log(Level.DEBUG, "Student has groupId, but group Id does not exist in Group base!");
					request.setAttribute(VARIABLE_FAIL_SHOW_STUDENT, message.getMessage("failed.groupIdNotFound"));
					return router;
				}
				session.setAttribute(ConstantManager.STUDENT_ATTRIBUTE, student);
				request.setAttribute(ConstantManager.STUDENT_EMAIL_ATTRIBUTE, user.getEmail());
				request.setAttribute(ConstantManager.GROUP_ATTRIBUTE, group);
				session.setAttribute(ConstantManager.STUDENT_ID_ATTRIBUTE, studentId);
				return router;
			} catch (ServiceException e) {
				throw new CommandException("Failed update student", e);
			}
		} else {
			LOGGER.log(Level.DEBUG, "Student ID invalid!");
			request.setAttribute(VARIABLE_FAIL_SHOW_STUDENT, message.getMessage("failed.invalidId"));
			return router;
		}
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(ConstantManager.LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
