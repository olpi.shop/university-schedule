package com.olpi.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.command.util.Validator;
import com.olpi.controller.Router;
import com.olpi.domain.Student;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.StudentService;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class AddStudentCommand implements Command {

	private StudentService studentService;
	private UserService userService;

	private static final Logger LOGGER = Logger.getLogger(AddStudentCommand.class);
	private static final String VARIABLE_FAIL_ADD_NEW_STUDENT = "failAddNewStudent";
	private static final String VARIABLE_SUCCESS_ADD_NEW_STUDENT = "successAddNewStudent";

	public AddStudentCommand(UserService userService, StudentService studentService) {
		this.studentService = studentService;
		this.userService = userService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("studentEmail");
		String password = request.getParameter("studentPassword");
		Long groupId = Long.valueOf(request.getParameter("group"));
		Router router = new Router();
		router.setPage(PathManager.JSP_ADMIN_ADD_STUDENT);

		if (!Validator.isEmailValid(email) && !Validator.isPasswordValid(password) && Validator.isNameValid(firstName)
				&& Validator.isNameValid(lastName)) {
			LOGGER.log(Level.DEBUG, "Pass or email invalid!");
			request.setAttribute(VARIABLE_FAIL_ADD_NEW_STUDENT, message.getMessage("failed.addStudent"));
			return router;
		}
		try {
			if (!userService.checkUserEmail(email)) {
				Student student = studentService.create(firstName, lastName, groupId);
				userService.register(email, student.getId(), UserRole.STUDENT, password);
				request.setAttribute(VARIABLE_SUCCESS_ADD_NEW_STUDENT,
						message.getMessage("success.student.added") + student.getId());
			} else {
				request.setAttribute(VARIABLE_FAIL_ADD_NEW_STUDENT, message.getMessage("failed.addStudent"));
			}
			LOGGER.log(Level.DEBUG, "Student was added!");
		} catch (ServiceException e) {
			throw new CommandException("Failed add new student", e);
		}
		return router;
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(ConstantManager.LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
