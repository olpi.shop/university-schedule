package com.olpi.command.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.PathManager;
import com.olpi.controller.Router;
import com.olpi.domain.DateSchedule;
import com.olpi.domain.Student;
import com.olpi.service.DateScheduleService;
import com.olpi.service.exceprion.ServiceException;

public class ShowScheduleCommand implements Command {

	private static final Logger LOGGER = Logger.getLogger(ShowScheduleCommand.class);

	private DateScheduleService dateService;
	private Integer pagesCount;

	public ShowScheduleCommand(DateScheduleService dateService) {
		this.dateService = dateService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		Router router = new Router();
		router.setPage(PathManager.JSP_STUDENT_SHOW_SCHEDULE);
		Integer pageNumber;
		String forward = request.getParameter(ConstantManager.FORWARD_PARAMETER);
		if (forward == null) {
			try {
				Student student = (Student) session.getAttribute(ConstantManager.STUDENT_ATTRIBUTE);
				Map<Integer, List<DateSchedule>> dateScheduleMap = createScheduleMap(request, student);
				pageNumber = 1;
				session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
				session.setAttribute(ConstantManager.DAY_SCHEDULE_MAP_ATTRIBUTE, dateScheduleMap);
				session.setAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE, pagesCount);
				LOGGER.log(Level.DEBUG, "Student schedule found!");
			} catch (ServiceException e) {
				throw new CommandException("Failed find schedule for student", e);
			}
		} else if (forward.equals("show_schedule_back")) {
			pageNumber = (Integer) session.getAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
			if (pageNumber > 1) {
				pageNumber = pageNumber - 1;
				session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
			}
		} else if (forward.equals("show_schedule_next")) {
			pageNumber = (Integer) session.getAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
			pagesCount = (Integer) session.getAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE);
			if (pageNumber < pagesCount) {
				pageNumber = pageNumber + 1;
				session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
			}
		}
		return router;
	}

	private Map<Integer, List<DateSchedule>> createScheduleMap(HttpServletRequest request, Student student)
			throws ServiceException {
		Map<Integer, List<DateSchedule>> dateScheduleMap = new HashMap<>();
		String period = request.getParameter(ConstantManager.PERIOD_PARAMETER);
		List<LocalDate> dates = transformPeriodInDateList(period);
		List<DateSchedule> dateSchedules = dateService.findScheduleForGroupByPeriod(dates, student.getGroupId());
		int countRecordsOnPage = 5;
		pagesCount = dateSchedules.size() / countRecordsOnPage;
		if (dateSchedules.size() % countRecordsOnPage != 0) {
			pagesCount += 1;
		}
		int index = 0;
		for (int i = 1; i < pagesCount + 1; i++) {
			int endIndex = ((dateSchedules.size() - index) < countRecordsOnPage) ? dateSchedules.size()
					: index + countRecordsOnPage;
			dateScheduleMap.put(i, dateSchedules.subList(index, endIndex));
			index += countRecordsOnPage;
		}
		return dateScheduleMap;
	}

	private List<LocalDate> transformPeriodInDateList(String periodInString) {
		String[] period = periodInString.split("-");
		int[] dateStart = Arrays.stream(period[0].split("/")).mapToInt(Integer::parseInt).toArray();
		int[] dateEnd = Arrays.stream(period[1].split("/")).mapToInt(Integer::parseInt).toArray();
		LocalDate start = LocalDate.of(dateStart[2], dateStart[1], dateStart[0]);
		LocalDate end = LocalDate.of(dateEnd[2], dateEnd[1], dateEnd[0]);
		return buildListOfDates(start, end);
	}
	
	private List<LocalDate> buildListOfDates(LocalDate start, LocalDate end) {
		List<LocalDate> dates = new ArrayList<>();
		while (!start.equals(end)) {
			dates.add(start);
			start = start.plusDays(1);
		}
		dates.add(end);
		return dates;
	}

}
