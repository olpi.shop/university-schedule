package com.olpi.command.impl;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.command.util.Validator;
import com.olpi.controller.Router;
import com.olpi.domain.Group;
import com.olpi.domain.Student;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.GroupService;
import com.olpi.service.StudentService;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class UpdateStudentCommand implements Command {

	private static final Logger LOGGER = Logger.getLogger(UpdateStudentCommand.class);

	private StudentService studentService;
	private UserService userService;
	private GroupService groupService;

	private static final String VARIABLE_FAIL_UPDATE = "invalidUpdateMessage";

	public UpdateStudentCommand(StudentService studentService, UserService userService, GroupService groupService) {
		this.studentService = studentService;
		this.userService = userService;
		this.groupService = groupService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);
		Router router = new Router();
		router.setPage(PathManager.JSP_ADMIN_UPDATE_STUDENT);
		try {
			Long studentId = (Long) session.getAttribute(ConstantManager.STUDENT_ID_ATTRIBUTE);
			Student student;
			Optional<Student> optionalStudent = studentService.findStudentById(studentId);
			if (optionalStudent.isPresent()) {
				student = optionalStudent.get();
			} else {
				LOGGER.log(Level.DEBUG, "Failed update student, Student Id not found");
				request.setAttribute(VARIABLE_FAIL_UPDATE, message.getMessage("failed.updateStudent"));
				return router;
			}

			if (!trySetFirstName(student, request) || !trySetLastName(student, request)
					|| !trySetGroupId(student, request)) {
				LOGGER.log(Level.DEBUG, "The first name or last name does not match the pattern!");
				request.setAttribute(VARIABLE_FAIL_UPDATE, message.getMessage("failed.updateStudent.invalidData"));
				return router;
			}

			Group group;
			User user;
			Optional<Group> optionalGroup = groupService.findGroupById(student.getGroupId());
			Optional<User> optionalUser = userService.findUserById(studentId, UserRole.STUDENT);
			if (optionalGroup.isPresent() && optionalUser.isPresent()) {
				group = optionalGroup.get();
				user = optionalUser.get();
			} else {
				LOGGER.log(Level.DEBUG, "The group id not found in DB!");
				request.setAttribute(VARIABLE_FAIL_UPDATE, message.getMessage("failed.updateStudent.invalidData"));
				return router;
			}

			String newStudentEmail = request.getParameter("newStudentEmail");
			if (Validator.isNotNullOrEmpty(newStudentEmail)) {
				if (Validator.isEmailValid(newStudentEmail) && !userService.checkUserEmail(newStudentEmail)) {
					userService.updateUserEmail(user, newStudentEmail);
					user.setEmail(newStudentEmail);
				} else {
					LOGGER.log(Level.DEBUG, "The email is invalid");
					request.setAttribute(VARIABLE_FAIL_UPDATE, message.getMessage("failed.updateStudent.email"));
					return router;
				}
			}
			student = studentService.update(student);

			request.setAttribute(ConstantManager.STUDENT_ATTRIBUTE, student);
			request.setAttribute(ConstantManager.STUDENT_EMAIL_ATTRIBUTE, user.getEmail());
			request.setAttribute(ConstantManager.GROUP_ATTRIBUTE, group);
		} catch (ServiceException e) {
			throw new CommandException("Failed update student", e);
		}
		return router;
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(ConstantManager.LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

	private boolean trySetFirstName(Student student, HttpServletRequest request) {
		String newFirstName = request.getParameter("newFirstName");
		if (!Validator.isNotNullOrEmpty(newFirstName)) {
			return true;
		} else if (Validator.isNameValid(newFirstName)) {
			student.setFirstName(newFirstName);
			return true;
		}
		return false;
	}

	private boolean trySetLastName(Student student, HttpServletRequest request) {
		String newLastName = request.getParameter("newLastName");
		if (!Validator.isNotNullOrEmpty(newLastName)) {
			return true;
		} else if (Validator.isNameValid(newLastName)) {
			student.setLastName(newLastName);
			return true;
		}
		return false;
	}

	private boolean trySetGroupId(Student student, HttpServletRequest request) {
		String id = request.getParameter("newGroupId");
		if (!Validator.isNotNullOrEmpty(id)) {
			return true;
		} else if (Validator.isLong(id)) {
			Long newGroupId = Long.valueOf(id);
			student.setGroupId(newGroupId);
			return true;
		}
		return false;
	}
}
