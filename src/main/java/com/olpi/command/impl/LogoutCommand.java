package com.olpi.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.PathManager;
import com.olpi.controller.Router;
import com.olpi.controller.Router.RouterType;

public class LogoutCommand implements Command {

	private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		Router router = new Router();
		request.getSession().invalidate();
		router.setType(RouterType.REDIRECT);
		router.setPage(PathManager.JSP_INDEX);
		LOGGER.log(Level.DEBUG, "User logged out.");
		return router;
	}

}
