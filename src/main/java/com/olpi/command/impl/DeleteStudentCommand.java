package com.olpi.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.controller.Router;
import com.olpi.domain.Student;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.StudentService;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class DeleteStudentCommand implements Command {

	private StudentService studentService;
	private UserService userService;

	private static final Logger LOGGER = Logger.getLogger(DeleteStudentCommand.class);
	private static final String VARIABLE_DELETE_STUDENT = "studentDeleteMessage";

	private static final String STUDENTS_ATTRIBUTE = "students";

	public DeleteStudentCommand(UserService userService, StudentService studentService) {
		this.studentService = studentService;
		this.userService = userService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);
		Router router = new Router();
		router.setPage(PathManager.JSP_ADMIN_DELETE_STUDENT);
		try {
			Long studentId = Long.valueOf(request.getParameter(ConstantManager.STUDENT_ID_ATTRIBUTE));
			userService.removeUser(studentId, UserRole.STUDENT);
			studentService.removeStudent(studentId);
			request.setAttribute(VARIABLE_DELETE_STUDENT, message.getMessage("success.deleteStudent"));
			List<Student> students = studentService.findAllStudents();
			session.setAttribute(STUDENTS_ATTRIBUTE, students);
			LOGGER.log(Level.DEBUG, "Student with id-" + studentId + " has been removed");
		} catch (ServiceException e) {
			throw new CommandException("Failed delete student", e);
		}
		return router;
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(ConstantManager.LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
