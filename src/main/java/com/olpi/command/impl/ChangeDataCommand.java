package com.olpi.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.command.util.Validator;
import com.olpi.controller.Router;
import com.olpi.domain.User;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class ChangeDataCommand implements Command {

	private static final Logger LOGGER = Logger.getLogger(ChangeDataCommand.class);

	private static final String VARIABLE_FAIL_CHANGE_DATA = "failChangeData";
	private static final String VARIABLE_WRONG_OLD_PASSWORD = "wrongOldPassword";
	private static final String VARIABLE_SUCCESS_CHANGE_DATA = "successChangeMessage";

	private UserService userService;

	public ChangeDataCommand(UserService userService) {
		this.userService = userService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);
		Router router = new Router();
		User user = (User) session.getAttribute(ConstantManager.USER_ATTRIBUTE);
		router.setPage(PathManager.JSP_CHANGE_PASS);

		String oldPassword = request.getParameter(ConstantManager.OLD_PASSWORD_PARAMETER);
		String newEmail = request.getParameter(ConstantManager.NEW_EMAIL_PARAMETER);
		String newPassword = request.getParameter(ConstantManager.NEW_PASSWORD_PARAMETER);
		String newPasswordRepeat = request.getParameter(ConstantManager.NEW_PASSWORD_REPEAT_PARAMETER);

		if (!Validator.isPasswordValid(oldPassword) || !Validator.isPasswordValid(newPassword)
				|| !Validator.isPasswordValid(newPasswordRepeat) || !(newPassword.equals(newPasswordRepeat))) {
			LOGGER.log(Level.DEBUG, "Invalide password");
			request.setAttribute(VARIABLE_FAIL_CHANGE_DATA, message.getMessage("failed.change.passMismatch"));
			return router;
		}
		if (!newEmail.isEmpty() && !Validator.isEmailValid(newEmail)) {
			System.out.println(newEmail);
			LOGGER.log(Level.DEBUG, "Invalide email");
			request.setAttribute(VARIABLE_FAIL_CHANGE_DATA, message.getMessage("failed.change.wrongEmail"));
			return router;
		}
		try {
			if (!userService.authorize(user.getEmail(), oldPassword).isPresent()) {
				LOGGER.log(Level.DEBUG, "Invalide old password");
				request.setAttribute(VARIABLE_WRONG_OLD_PASSWORD, message.getMessage("failed.change.wrongOldPass"));
				return router;
			}
			if (Validator.isNotNullOrEmpty(newEmail) && !newEmail.equals(user.getEmail())) {
				if (userService.checkUserEmail(newEmail)) {
					LOGGER.log(Level.DEBUG, "New e-mail already exist in data base!");
					request.setAttribute(VARIABLE_FAIL_CHANGE_DATA, message.getMessage("failed.change.newEmailExist"));
					return router;
				}
				userService.updateUser(user, newEmail, newPassword);
				user.setEmail(newEmail);
				session.setAttribute(ConstantManager.USER_ATTRIBUTE, user);
			} else {
				userService.updateUserPassword(user, newPassword);
			}
			request.setAttribute(VARIABLE_SUCCESS_CHANGE_DATA, message.getMessage("success.changeData"));
		} catch (ServiceException e) {
			throw new CommandException("Failed change email or password", e);
		}
		return router;
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(ConstantManager.LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
