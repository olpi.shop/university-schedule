package com.olpi.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.PathManager;
import com.olpi.controller.Router;

public class LocalCommand implements Command {

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		Router router = new Router();
		HttpSession session = request.getSession(true);
		session.setAttribute(ConstantManager.LOCALE_ATTRIBUTE, request.getParameter(ConstantManager.LOCALE_ATTRIBUTE));
		router.setPage(PathManager.JSP_LOGIN);
		return router;
	}

}
