package com.olpi.command.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.ConstantManager;
import com.olpi.command.util.PathManager;
import com.olpi.controller.Router;
import com.olpi.controller.Router.RouterType;
import com.olpi.domain.Group;
import com.olpi.domain.Student;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.GroupService;
import com.olpi.service.StudentService;
import com.olpi.service.exceprion.ServiceException;

public class ForwardPageCommand implements Command {

	private GroupService groupService;
	private StudentService studentService;

	public ForwardPageCommand(StudentService studentService, GroupService groupService) {
		this.studentService = studentService;
		this.groupService = groupService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		Router router = new Router();
		String page = request.getParameter(ConstantManager.FORWARD_PARAMETER);
		switch (page) {
		case "add_user":
			addGroupsInSession(request);
			router.setPage(PathManager.JSP_ADMIN_ADD_STUDENT);
			return router;
		case "update_user":
			addGroupsInSession(request);
			router.setPage(PathManager.JSP_ADMIN_UPDATE_STUDENT);
			return router;
		case "delete_user":
			addStudentsInSession(request);
			router.setPage(PathManager.JSP_ADMIN_DELETE_STUDENT);
			return router;
		case "show_all_students":
			addStudentsMapInSession(request);
			router.setPage(PathManager.JSP_ADMIN_SHOW_ALL_STUDENTS);
			return router;
		case "show_all_students_back":
			decreasePageNumberInSession(request);
			router.setPage(PathManager.JSP_ADMIN_SHOW_ALL_STUDENTS);
			return router;
		case "show_all_students_next":
			increasePageNumberInSession(request);
			router.setPage(PathManager.JSP_ADMIN_SHOW_ALL_STUDENTS);
			return router;
		case "menu":
			HttpSession session = request.getSession(true);
			User user = (User) session.getAttribute("user");
			if (user.getRole().equals(UserRole.ADMIN)) {
				session.removeAttribute(ConstantManager.STUDENT_ATTRIBUTE);
				session.removeAttribute(ConstantManager.GROUPS_ATTRIBUTE);
				session.removeAttribute(ConstantManager.STUDENTS_ATTRIBUTE);
				session.removeAttribute(ConstantManager.STUDENT_MAP_ATTRIBUTE);
				session.removeAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
				session.removeAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE);
				router.setPage(PathManager.JSP_ADMIN_MENU);
			} else if (user.getRole().equals(UserRole.STUDENT)) {
				session.removeAttribute(ConstantManager.DAY_SCHEDULE_MAP_ATTRIBUTE);
				session.removeAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
				session.removeAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE);
				router.setPage(PathManager.JSP_STUDENT_MENU);
			} else {
				// add role
			}
			return router;
		case "change_pass":
			router.setPage(PathManager.JSP_CHANGE_PASS);
			return router;
		case "show_schedule":
			router.setPage(PathManager.JSP_STUDENT_SHOW_SCHEDULE);
			return router;
		default:
			router.setPage(PathManager.JSP_INDEX);
			router.setType(RouterType.REDIRECT);
			return router;
		}
	}

	private void addGroupsInSession(HttpServletRequest request) throws CommandException {
		try {
			List<Group> groups = groupService.findAllGroupsWithoutStudents();
			HttpSession session = request.getSession(true);
			session.setAttribute(ConstantManager.GROUPS_ATTRIBUTE, groups);
		} catch (ServiceException e) {
			throw new CommandException("When add new student", e);
		}
	}

	private void addStudentsInSession(HttpServletRequest request) throws CommandException {
		try {
			List<Student> students = studentService.findAllStudents();
			HttpSession session = request.getSession(true);
			session.setAttribute(ConstantManager.STUDENTS_ATTRIBUTE, students);
		} catch (ServiceException e) {
			throw new CommandException("When update student", e);
		}
	}

	private void addStudentsMapInSession(HttpServletRequest request) throws CommandException {
		try {
			List<String> studentsToString = createStudentsToStringList();

			Map<Integer, List<String>> studentsMap = new HashMap<>();
			int countRecordsOnPage = 20;
			Integer pagesCount = studentsToString.size() / countRecordsOnPage + 1;

			int index = 0;
			for (int i = 1; i < pagesCount + 1; i++) {
				int endIndex = ((studentsToString.size() - index) < countRecordsOnPage) ? studentsToString.size()
						: index + countRecordsOnPage;
				studentsMap.put(i, studentsToString.subList(index, endIndex));
				index += countRecordsOnPage;
			}
			HttpSession session = request.getSession(true);
			session.setAttribute(ConstantManager.STUDENT_MAP_ATTRIBUTE, studentsMap);
			Integer pageNumber = 1;
			session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
			session.setAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE, pagesCount);
		} catch (ServiceException e) {
			throw new CommandException("When show all students", e);
		}
	}

	private List<String> createStudentsToStringList() throws ServiceException {
		List<String> studentsToString = new ArrayList<>();
		List<Group> groups = groupService.findAllGroups();
		for (Group group : groups) {
			List<Student> students = group.getStudents();
			for (Student student : students) {
				studentsToString.add(student.toString() + group.toString());
			}
		}
		return studentsToString;
	}

	private void increasePageNumberInSession(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Integer pageNumber = (Integer) session.getAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
		Integer pagesCount = (Integer) session.getAttribute(ConstantManager.PAGES_COUNT_ATTRIBUTE);

		if ((pageNumber != null && pagesCount != null) && pageNumber < pagesCount) {
			pageNumber = pageNumber + 1;
			session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
		}
	}

	private void decreasePageNumberInSession(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Integer pageNumber = (Integer) session.getAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE);
		if ((pageNumber != null) && pageNumber > 1) {
			pageNumber = pageNumber - 1;
			session.setAttribute(ConstantManager.PAGE_NUMBER_ATTRIBUTE, pageNumber);
		}
	}

}
