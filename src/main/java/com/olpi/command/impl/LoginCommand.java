package com.olpi.command.impl;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.olpi.command.Command;
import com.olpi.command.exception.CommandException;
import com.olpi.command.util.MessageManager;
import com.olpi.command.util.PathManager;
import com.olpi.command.util.Validator;
import com.olpi.controller.Router;
import com.olpi.domain.Student;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;
import com.olpi.service.StudentService;
import com.olpi.service.UserService;
import com.olpi.service.exceprion.ServiceException;

public class LoginCommand implements Command {

	private UserService userService;
	private StudentService studentService;

	private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

	private static final String EMAIL = "email";
	private static final String PASSWORD = "password";
	private static final String LOCALE_ATTRIBUTE = "locale";
	private static final String USER_ATTRIBUTE = "user";
	private static final String STUDENT_ATTRIBUTE = "student";

	private static final String VARIABLE_FAIL_LOG_IN = "failLoginMessage";

	public LoginCommand(UserService userService, StudentService studentService) {
		this.userService = userService;
		this.studentService = studentService;
	}

	@Override
	public Router execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, CommandException {
		HttpSession session = request.getSession(true);
		MessageManager message = getMessageManager(session);
		Router router = new Router();
		router.setPage(PathManager.JSP_LOGIN);

		String email = request.getParameter(EMAIL);
		String password = request.getParameter(PASSWORD);
		if (!Validator.isEmailValid(email) && !Validator.isPasswordValid(password)) {
			LOGGER.log(Level.DEBUG, "Pass or email invalid!");
			request.setAttribute(VARIABLE_FAIL_LOG_IN, message.getMessage("failed.login"));
			return router;
		}
		try {
			Optional<User> optionalUser = userService.authorize(email, password);
			if (!optionalUser.isPresent()) {
				LOGGER.log(Level.DEBUG, "user not found!");
				request.setAttribute(VARIABLE_FAIL_LOG_IN, message.getMessage("failed.login"));
				return router;
			}
			User user = optionalUser.get();
			session.setAttribute(USER_ATTRIBUTE, user);
			UserRole role = user.getRole();
			if (role.equals(UserRole.STUDENT)) {
				Optional<Student> opStudent = studentService.findStudentById(user.getId());
				if (!opStudent.isPresent()) {
					request.setAttribute(VARIABLE_FAIL_LOG_IN, message.getMessage("failed.studentDoesNotExist"));
					return router;
				}
				Student student = opStudent.get();
				session.setAttribute(STUDENT_ATTRIBUTE, student);
				router.setPage(PathManager.JSP_STUDENT_MENU);
			} else if (role.equals(UserRole.ADMIN)) {
				router.setPage(PathManager.JSP_ADMIN_MENU);
			} else {
				// add role
			}
		} catch (ServiceException e) {
			throw new CommandException("Failed login", e);
		}
		return router;
	}

	private MessageManager getMessageManager(HttpSession session) {
		String locale = String.valueOf(session.getAttribute(LOCALE_ATTRIBUTE)).toUpperCase();
		if (locale.equals("NULL")) {
			return MessageManager.RU_RU;
		}
		return MessageManager.valueOf(locale);
	}

}
