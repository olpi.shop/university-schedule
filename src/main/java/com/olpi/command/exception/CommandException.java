package com.olpi.command.exception;

@SuppressWarnings("serial")
public class CommandException extends Exception {

	public CommandException() {
		super();
	}

	public CommandException(String message) {
		super(message);
	}

	public CommandException(String message, Throwable cause) {
		super(message, cause);
	}

}
