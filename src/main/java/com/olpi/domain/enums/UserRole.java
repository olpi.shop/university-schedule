package com.olpi.domain.enums;

public enum UserRole {
	STUDENT, 
	TEACHER, 
	ADMIN;
}
