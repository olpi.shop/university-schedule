package com.olpi.domain.enums;

public enum Department {

	ECONOMICS("Economics"), ECOLOGY("Ecology"), PHILOSOPHY("Philosophy"), MATH("Math"),
	INFORMATION_SYSTEMS("Information_Systems");

	private String name;

	Department(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
