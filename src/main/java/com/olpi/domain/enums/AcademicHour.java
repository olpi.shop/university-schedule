package com.olpi.domain.enums;

public enum AcademicHour {
    FIRST("08.30-09.50"),
    SECOND("10.00-11.20"),
    THIRD("11.30-12.50"),
    FOURTH("13.10-14.30"),
    FIVETH("14.40-16.00"),
    SIXTH("16.10-17.30"),
    SEVENTH("17.40-19.00"),
    EIGHTH("08.30-09.50"),
    NINETH("19.10-20.30");
    
	private String time;

	AcademicHour(String time) {
		this.time = time;
	}

	public String getTime() {
		return time;
	}

}
