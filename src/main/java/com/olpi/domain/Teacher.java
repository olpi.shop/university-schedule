package com.olpi.domain;

import java.util.Objects;
import com.olpi.domain.enums.Department;

public class Teacher extends AbstractPerson {

	private static final long serialVersionUID = -1278715996947501206L;

	private Department department;
	
	public static class Builder {

		private Long id;
		private String firstName;
		private String lastName;
		private Department department;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder deparment(Department department) {
			this.department = department;
			return this;
		}

		public Teacher build() {
			return new Teacher(this);
		}
	}

	private Teacher(Builder builder) {
		super(builder.firstName, builder.lastName);
		this.setId(builder.id);
		this.department = builder.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Department getDepartment() {
		return department;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Teacher other = (Teacher) obj;
		return getId() == other.getId() && getFirstName().equals(other.getFirstName())
				&& getLastName().equals(other.getLastName()) && department.equals(other.department);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getFirstName(), getLastName(), department);
	}

	@Override
	public String toString() {
		return "Teacher: " + super.toString() + " department: " + department.getName();
	}

}
