package com.olpi.domain;

import java.util.Objects;

public class Student extends AbstractPerson {

	private static final long serialVersionUID = 3449602712895457356L;

	private Long groupId;

	public static class Builder {
		private Long id;
		private String firstName;
		private String lastName;
		private Long groupId;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder groupId(Long groupId) {
			this.groupId = groupId;
			return this;
		}

		public Student build() {
			return new Student(this);
		}
	}

	private Student(Builder builder) {

		super(builder.firstName, builder.lastName);
		this.setId(builder.id);
		this.groupId = builder.groupId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Student other = (Student) obj;
		return getId() == other.getId() && getFirstName().equals(other.getFirstName())
				&& getLastName().equals(other.getLastName()) && getGroupId() == other.getGroupId();
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getFirstName(), getLastName(), getGroupId());
	}

	@Override
	public String toString() {
		return super.toString() + " ";
	}

}
