package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;

import com.olpi.domain.enums.Department;

public class Subject extends Entity {

	private static final long serialVersionUID = 942686327389519646L;

	private Long id;
	private String name;
	private Department deparment;
	private List<Teacher> teachers;

	public static class Builder {

		private Long id;
		private String name;
		private Department deparment;
		private List<Teacher> teachers;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder deparment(Department deparment) {
			this.deparment = deparment;
			return this;
		}

		public Subject build() {
			this.teachers = new ArrayList<>();
			return new Subject(this);
		}
	}

	private Subject(Builder builder) {

		this.id = builder.id;
		this.name = builder.name;
		this.deparment = builder.deparment;
		this.teachers = builder.teachers;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTeachers(Teacher teacher) {
		this.teachers.add(teacher);
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public void setDepartment(Department deparment) {
		this.deparment = deparment;
	}

	public Department getDepartment() {
		return deparment;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deparment == null) ? 0 : deparment.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((teachers == null) ? 0 : teachers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		if (deparment != other.deparment)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (teachers == null) {
			if (other.teachers != null)
				return false;
		} else if (!teachers.equals(other.teachers))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Subject: id = " + id + ", name: " + name + " department: " + deparment.getName();
	}

}
