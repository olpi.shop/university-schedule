package com.olpi.domain;

import com.olpi.domain.enums.AcademicHour;
import com.olpi.domain.enums.Classroom;

public class Schedule extends Entity {

	private static final long serialVersionUID = -569780676869613501L;

	private Long id;
	private Lesson lesson;
	private Classroom room;
	private AcademicHour hour;

	public static class Builder {

		private Long id;
		private Lesson lesson;
		private Classroom room;
		private AcademicHour hour;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder lesson(Lesson lesson) {
			this.lesson = lesson;
			return this;
		}

		public Builder room(Classroom room) {
			this.room = room;
			return this;
		}

		public Builder hour(AcademicHour hour) {
			this.hour = hour;
			return this;
		}

		public Schedule build() {
			return new Schedule(this);
		}
	}

	private Schedule(Builder builder) {
		this.id = builder.id;
		this.lesson = builder.lesson;
		this.room = builder.room;
		this.hour = builder.hour;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}

	public void setRoom(Classroom room) {
		this.room = room;
	}

	public void setHour(AcademicHour hour) {
		this.hour = hour;
	}

	public Long getId() {
		return id;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public Classroom getRoom() {
		return room;
	}

	public AcademicHour getHour() {
		return hour;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hour == null) ? 0 : hour.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lesson == null) ? 0 : lesson.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schedule other = (Schedule) obj;
		if (hour != other.hour)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lesson == null) {
			if (other.lesson != null)
				return false;
		} else if (!lesson.equals(other.lesson))
			return false;
		if (room != other.room)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(" %s %s | %s |", lesson.toString(), room.getNubmer(), hour.getTime());
	}

}
