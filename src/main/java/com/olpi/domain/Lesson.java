package com.olpi.domain;

public class Lesson extends Entity {

	private static final long serialVersionUID = -2415037385596374377L;

	private Long id;
	private Group group;
	private Subject subject;
	private Teacher teacher;

	public static class Builder {
		private Long id;
		private Group group;
		private Subject subject;
		private Teacher teacher;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder group(Group group) {
			this.group = group;
			return this;
		}

		public Builder subject(Subject subject) {
			this.subject = subject;
			return this;
		}

		public Builder teacher(Teacher teacher) {
			this.teacher = teacher;
			return this;
		}

		public Lesson build() {
			return new Lesson(this);
		}
	}

	private Lesson(Builder builder) {

		this.id = builder.id;
		this.group = builder.group;
		this.subject = builder.subject;
		this.teacher = builder.teacher;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Long getId() {
		return id;
	}

	public Group getGroup() {
		return group;
	}

	public Subject getSubject() {
		return subject;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((teacher == null) ? 0 : teacher.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lesson other = (Lesson) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (teacher == null) {
			if (other.teacher != null)
				return false;
		} else if (!teacher.equals(other.teacher))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%-25s | %s-%d-%d | %s %s |", subject.getName(), group.getName(), group.getCourse(),
				group.getStream(), teacher.getLastName(), teacher.getFirstName());
	}

}
