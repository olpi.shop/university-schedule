package com.olpi.domain;

import com.olpi.domain.enums.UserRole;

public class User extends Entity {

	private static final long serialVersionUID = -6854044181949222730L;
	private String email;
	private Long id;
	private UserRole role;

	public User(String email, Long id, UserRole role) {
		this.email = email;
		this.id = id;
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public UserRole getRole() {
		return role;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (role != other.role)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", id=" + id + ", role=" + role + "]";
	}
}
