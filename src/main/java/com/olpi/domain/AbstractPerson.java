package com.olpi.domain;

@SuppressWarnings("serial")
public abstract class AbstractPerson extends Entity {

	private Long id;
	private String firstName;
	private String lastName;

	public AbstractPerson(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public abstract boolean equals(Object obj);

	public abstract int hashCode();

	@Override
	public String toString() {
		return "Name: " + firstName + " " + lastName + ", id-" + id;
	}

}
