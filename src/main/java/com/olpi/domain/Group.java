package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;

public class Group extends Entity {

	private static final long serialVersionUID = 3116749287112613330L;

	private Long id;
	private String name;
	private int course;
	private int stream;
	private List<Student> students;

	public static class Builder {

		private Long id;
		private String name;
		private int course;
		private int stream;
		private List<Student> students;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder course(int course) {
			this.course = course;
			return this;
		}

		public Builder stream(int stream) {
			this.stream = stream;
			return this;
		}

		public Group build() {
			students = new ArrayList<>();
			return new Group(this);
		}
	}

	private Group(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.course = builder.course;
		this.stream = builder.stream;
		this.students = builder.students;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public void setStream(int stream) {
		this.stream = stream;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public void setStudent(Student student) {
		students.add(student);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getCourse() {
		return course;
	}

	public int getStream() {
		return stream;
	}

	public List<Student> getStudents() {
		return students;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + course;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + stream;
		result = prime * result + ((students == null) ? 0 : students.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (course != other.course)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (stream != other.stream)
			return false;
		if (students == null) {
			if (other.students != null)
				return false;
		} else if (!students.equals(other.students))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name + "-" + course + "-" + stream;
	}

}
