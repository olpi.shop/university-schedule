package com.olpi.domain;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public class WeekDaySchedule extends Entity {

	private static final long serialVersionUID = 4474438911757897772L;

	private List<Schedule> schedules;
	private DayOfWeek dayId;

	public WeekDaySchedule(DayOfWeek dayId) {
		this.dayId = dayId;
		schedules = new ArrayList<>();
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public void addSchedule(Schedule schedule) {
		this.schedules.add(schedule);
	}

	public DayOfWeek getDayId() {
		return dayId;
	}

	public List<Schedule> getSchedule() {
		return schedules;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dayId == null) ? 0 : dayId.hashCode());
		result = prime * result + ((schedules == null) ? 0 : schedules.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeekDaySchedule other = (WeekDaySchedule) obj;
		if (dayId != other.dayId)
			return false;
		if (schedules == null) {
			if (other.schedules != null)
				return false;
		} else if (!schedules.equals(other.schedules))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WeekDaySchedule [schedules=" + schedules + ", dayId=" + dayId + "]";
	}

}
