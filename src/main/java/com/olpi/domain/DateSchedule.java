package com.olpi.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
public class DateSchedule extends Entity {

	private static final long serialVersionUID = -136705124085535919L;

	private LocalDate dateId;
	private List<Schedule> schedules;

	public DateSchedule(LocalDate dateId) {
		this.dateId = dateId;
		schedules = new ArrayList<>();
	}

	public LocalDate getDateId() {
		return dateId;
	}

	public List<Schedule> getSchedules() {
		return schedules;
	}

	public void setDateId(LocalDate dateId) {
		this.dateId = dateId;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public void addSchedule(Schedule schedule) {
		this.schedules.add(schedule);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateId == null) ? 0 : dateId.hashCode());
		result = prime * result + ((schedules == null) ? 0 : schedules.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateSchedule other = (DateSchedule) obj;
		if (dateId == null) {
			if (other.dateId != null)
				return false;
		} else if (!dateId.equals(other.dateId))
			return false;
		if (schedules == null) {
			if (other.schedules != null)
				return false;
		} else if (!schedules.equals(other.schedules))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return dateId.format(DateTimeFormatter.ISO_DATE);
	}
	
	public String toString(String locale) {
		if(locale.equals("en_US")) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			return dateId.format(formatter);
		}
		else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			return dateId.format(formatter);
		}
	}

}
