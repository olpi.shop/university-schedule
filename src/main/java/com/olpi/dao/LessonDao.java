package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Lesson;

public abstract class LessonDao extends AbstractDao<Long, Lesson> {

	public abstract Lesson createOrUpdate(Lesson lesson) throws DAOException;

	public abstract Optional<Lesson> findById(Long lessonId) throws DAOException;

	public abstract List<Lesson> findAll() throws DAOException;

	public abstract boolean remove(Long lessonId) throws DAOException;

	public abstract void replaceTeacher(Long lessonId, Long newTeacherId) throws DAOException;

	public abstract void removeAll() throws DAOException;

}
