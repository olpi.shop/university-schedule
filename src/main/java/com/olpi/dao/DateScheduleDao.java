package com.olpi.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.DateSchedule;
import com.olpi.domain.Schedule;

public abstract class DateScheduleDao extends AbstractDao<LocalDate, DateSchedule> {

	public abstract DateSchedule createOrUpdate(DateSchedule dateSchedule) throws DAOException;

	public abstract Optional<DateSchedule> findById(LocalDate dateId) throws DAOException;

	public abstract List<DateSchedule> findAll() throws DAOException;

	public abstract boolean remove(LocalDate dateId) throws DAOException;

	public abstract void removeAll() throws DAOException;

	public abstract List<LocalDate> findAllDates() throws DAOException;

	public abstract DateSchedule findScheduleByGroupId(LocalDate dateId, Long groupId) throws DAOException;

	public abstract DateSchedule findScheduleByTeacherId(LocalDate dateId, Long teacherId) throws DAOException;

	public abstract void addScheduleByDate(LocalDate dateId, Schedule schedule) throws DAOException;

	public abstract void addScheduleByDate(LocalDate dateId, List<Schedule> schedules) throws DAOException;

}
