package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Student;

public abstract class StudentDao extends AbstractDao<Long, Student> {

	public abstract Student createOrUpdate(Student student) throws DAOException;

	public abstract Optional<Student> findById(Long studentId) throws DAOException;

	public abstract List<Student> findAll() throws DAOException;

	public abstract boolean remove(Long studentId) throws DAOException;

	public abstract boolean checkStudentId(Long id) throws DAOException;

}
