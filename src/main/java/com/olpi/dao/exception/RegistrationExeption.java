package com.olpi.dao.exception;

@SuppressWarnings("serial")
public class RegistrationExeption extends Exception {

	public RegistrationExeption() {
		super();
	}

	public RegistrationExeption(String message) {
		super(message);
	}

	public RegistrationExeption(String message, Throwable cause) {
		super(message, cause);
	}
}
