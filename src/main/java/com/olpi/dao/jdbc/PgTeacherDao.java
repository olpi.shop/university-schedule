package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.TeacherDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.Department;

public class PgTeacherDao extends TeacherDao {

	private static final String SQL_INSERT_NEW_TEACHER = "INSERT INTO teachers (first_name, last_name, department) VALUES (?, ?, ?::department) RETURNING teacher_id;";
	private static final String SQL_SELECT_TEACHER_ID = "SELECT teacher_id FROM teachers WHERE teacher_id = ?;";
	private static final String SQL_UPDATE_TEACHER = "UPDATE teachers SET first_name = ?, last_name = ?, department = ?::department WHERE teacher_id = ?;";
	private static final String SQL_SELECT_TEACHER = "SELECT first_name, last_name, department FROM teachers  WHERE teacher_id = ?;";
	private static final String SQL_SELECT_ALL_TEACHERS = "SELECT teacher_id, first_name, last_name, department FROM teachers;";
	private static final String SQL_SELECT_ALL_TEACHERS_BY_DEPARTMENT = "SELECT teacher_id, first_name, last_name FROM teachers WHERE department = ?::department;";
	private static final String SQL_DELETE_TEACHER = "DELETE FROM teachers WHERE teacher_id = ?;";
	private static final String SQL_SELECT_LESSONS_BY_TEACHER_ID = "SELECT lesson_id FROM lessons WHERE teacher_id = ?;";

	@Override
	public Teacher createOrUpdate(Teacher teacher) throws DAOException {
		if (teacher.getId() == null) {
			return create(teacher);
		} else if (checkTeacherId(teacher.getId())) {
			return update(teacher);
		} else {
			throw new DAOException("The teacher has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Teacher> findById(Long teacherId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TEACHER)) {
			statement.setLong(1, teacherId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			String firstName = resultSet.getString("first_name");
			String lastName = resultSet.getString("last_name");
			Department department = Department.valueOf(resultSet.getString("department"));
			Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
					.deparment(department).build();
			return Optional.of(teacher);
		} catch (SQLException e) {
			throw new DAOException("Failed to find teacher by id!", e);
		}
	}

	@Override
	public List<Teacher> findAll() throws DAOException {
		List<Teacher> teachers = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_TEACHERS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long teacherId = resultSet.getLong("teacher_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Department department = Department.valueOf(resultSet.getString("department"));
				Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
						.deparment(department).build();
				teachers.add(teacher);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all teachers!", e);
		}
		return teachers;
	}

	@Override
	public List<Teacher> findAllByDepartment(Department department) throws DAOException {
		List<Teacher> teachers = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_TEACHERS_BY_DEPARTMENT)) {
			statement.setString(1, department.name());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long teacherId = resultSet.getLong("teacher_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
						.deparment(department).build();
				teachers.add(teacher);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all teachers!", e);
		}
		return teachers;
	}

	@Override
	public boolean remove(Long teacherId) throws DAOException {
		if (!checkLessonsByTeacherId(teacherId)) {
			try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_TEACHER)) {
				statement.setLong(1, teacherId);
				statement.executeUpdate();
				return !checkTeacherId(teacherId);
			} catch (SQLException e) {
				throw new DAOException("Failed to remove teacher!", e);
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean checkTeacherId(Long id) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TEACHER_ID)) {
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check teacherId!", e);
		}
	}

	private Teacher create(Teacher teacher) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_TEACHER)) {
			statement.setString(1, teacher.getFirstName());
			statement.setString(2, teacher.getLastName());
			statement.setString(3, teacher.getDepartment().name());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			teacher.setId(resultSet.getLong("teacher_id"));
		} catch (SQLException e) {
			throw new DAOException("Failed to create teacher!", e);
		}
		return teacher;
	}

	private Teacher update(Teacher teacher) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TEACHER)) {
			statement.setString(1, teacher.getFirstName());
			statement.setString(2, teacher.getLastName());
			statement.setString(3, teacher.getDepartment().name());
			statement.setLong(4, teacher.getId());
			statement.executeUpdate();
			return teacher;
		} catch (SQLException e) {
			throw new DAOException("Failed to update teacher!", e);
		}
	}

	private boolean checkLessonsByTeacherId(Long teacherId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LESSONS_BY_TEACHER_ID)) {
			statement.setLong(1, teacherId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check lessons by teacherId!", e);
		}
	}

}
