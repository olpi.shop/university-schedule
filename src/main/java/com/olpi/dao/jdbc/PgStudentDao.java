package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.StudentDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Student;

public class PgStudentDao extends StudentDao {

	private static final String SQL_INSERT_NEW_STUDENT = "INSERT INTO students (first_name, last_name, group_id) VALUES (?,?,?) RETURNING student_id;";
	private static final String SQL_SELECT_STUDENT_ID = "SELECT student_id FROM students WHERE student_id = ?;";
	private static final String SQL_UPDATE_STUDENT = "UPDATE  students SET first_name = ?, last_name = ?, group_id = ? WHERE student_id = ?;";
	private static final String SQL_SELECT_STUDENT = "SELECT first_name, last_name, group_id FROM students  WHERE student_id = ?;";
	private static final String SQL_SELECT_ALL_STUDENTS = "SELECT student_id, first_name, last_name, group_id FROM students";
	private static final String SQL_DELETE_STUDENT = "DELETE FROM students WHERE student_id = ?;";

	@Override
	public Student createOrUpdate(Student student) throws DAOException {
		if (student.getId() == null) {
			return create(student);
		} else if (checkStudentId(student.getId())) {
			return update(student);
		} else {
			throw new DAOException("The student has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Student> findById(Long studentId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_STUDENT)) {
			statement.setLong(1, studentId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			String firstName = resultSet.getString("first_name");
			String lastName = resultSet.getString("last_name");
			Long groupId = resultSet.getLong("group_id");

			Student student = new Student.Builder().id(studentId).firstName(firstName).lastName(lastName)
					.groupId(groupId).build();
			return Optional.of(student);
		} catch (SQLException e) {
			throw new DAOException("Failed to find student by id!", e);
		}
	}

	@Override
	public List<Student> findAll() throws DAOException {
		List<Student> students = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_STUDENTS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long studentId = resultSet.getLong("student_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Long groupId = resultSet.getLong("group_id");

				Student student = new Student.Builder().id(studentId).firstName(firstName).lastName(lastName)
						.groupId(groupId).build();
				students.add(student);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all students!", e);
		}
		return students;
	}

	@Override
	public boolean remove(Long studentId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_STUDENT)) {
			statement.setLong(1, studentId);
			statement.executeUpdate();
			return !checkStudentId(studentId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove student!", e);
		}
	}

	@Override
	public boolean checkStudentId(Long id) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_STUDENT_ID)) {
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check studentId!", e);
		}
	}

	private Student create(Student student) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_STUDENT)) {
			statement.setString(1, student.getFirstName());
			statement.setString(2, student.getLastName());
			statement.setLong(3, student.getGroupId());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			student.setId(resultSet.getLong("student_id"));
		} catch (SQLException e) {
			throw new DAOException("Failed to create student!", e);
		}
		return student;
	}

	private Student update(Student student) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_STUDENT)) {
			statement.setString(1, student.getFirstName());
			statement.setString(2, student.getLastName());
			statement.setLong(3, student.getGroupId());
			statement.setLong(4, student.getId());
			statement.executeUpdate();
			return student;
		} catch (SQLException e) {
			throw new DAOException("Failed to update student!", e);
		}
	}

}
