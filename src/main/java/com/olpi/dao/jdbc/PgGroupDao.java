package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.GroupDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Group;
import com.olpi.domain.Student;

public class PgGroupDao extends GroupDao {

	private static final String SQL_INSERT_NEW_GROUP = "INSERT INTO groups (group_name, course, stream) VALUES (?,?,?) RETURNING group_id;";
	private static final String SQL_SELECT_GROUP_ID = "SELECT group_id FROM groups WHERE group_id = ?;";
	private static final String SQL_UPDATE_GROUP = "UPDATE  groups SET group_name = ?, course = ?, stream = ? WHERE group_id = ?;";
	private static final String SQL_SELECT_GROUP = "SELECT group_name, course, stream FROM groups  WHERE group_id = ?;";
	private static final String SQL_SELECT_ALL_GROUPS = "SELECT group_id, group_name, course, stream FROM groups;";
	private static final String SQL_DELETE_GROUP = "DELETE FROM groups WHERE group_id = ?;";
	private static final String SQL_SELECT_STUDENTS_BY_GROUP_ID = "SELECT student_id, first_name, last_name FROM students WHERE group_id = ?;";

	@Override
	public Group createOrUpdate(Group group) throws DAOException {
		if (group.getId() == null) {
			return create(group);
		} else if (checkGroupId(group.getId())) {
			return update(group);
		} else {
			throw new DAOException("The group has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Group> findById(Long groupId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GROUP)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			String groupName = resultSet.getString("group_name");
			int course = resultSet.getInt("course");
			int stream = resultSet.getInt("stream");
			List<Student> students = findStudentsByGroupId(groupId);
			Group group = new Group.Builder().id(groupId).name(groupName).course(course).stream(stream).build();
			group.setStudents(students);
			return Optional.of(group);
		} catch (SQLException e) {
			throw new DAOException("Failed to find group by id!", e);
		}
	}

	@Override
	public List<Group> findAll() throws DAOException {
		List<Group> groups = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_GROUPS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long groupId = resultSet.getLong("group_id");
				String groupName = resultSet.getString("group_name");
				int course = resultSet.getInt("course");
				int stream = resultSet.getInt("stream");
				List<Student> students = findStudentsByGroupId(groupId);

				Group group = new Group.Builder().id(groupId).name(groupName).course(course).stream(stream).build();
				group.setStudents(students);
				groups.add(group);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all groups!", e);
		}
		return groups;
	}

	@Override
	public List<Group> findAllGroupsWithoutStudents() throws DAOException {
		List<Group> groups = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_GROUPS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long groupId = resultSet.getLong("group_id");
				String groupName = resultSet.getString("group_name");
				int course = resultSet.getInt("course");
				int stream = resultSet.getInt("stream");
				Group group = new Group.Builder().id(groupId).name(groupName).course(course).stream(stream).build();
				groups.add(group);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all groups without students!", e);
		}
		return groups;
	}

	@Override
	public Optional<Group> findGroupWithoutStudents(Long groupId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GROUP)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			String groupName = resultSet.getString("group_name");
			int course = resultSet.getInt("course");
			int stream = resultSet.getInt("stream");
			Group group = new Group.Builder().id(groupId).name(groupName).course(course).stream(stream).build();
			return Optional.of(group);
		} catch (SQLException e) {
			throw new DAOException("Failed to find group by id!", e);
		}
	}

	@Override
	public boolean remove(Long groupId) throws DAOException {
		if (!checkStudentsByGroupId(groupId)) {
			try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_GROUP);) {
				statement.setLong(1, groupId);
				statement.executeUpdate();
				return !checkGroupId(groupId);
			} catch (SQLException e) {
				throw new DAOException("Failed to find all groups!", e);
			}
		} else {
			return false;
		}
	}

	@Override
	public List<Student> findStudentsByGroupId(Long groupId) throws DAOException {
		List<Student> students = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_STUDENTS_BY_GROUP_ID)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long studentId = resultSet.getLong("student_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				students.add(new Student.Builder().id(studentId).firstName(firstName).lastName(lastName)
						.groupId(groupId).build());
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all students in group!", e);
		}
		return students;
	}

	@Override
	public boolean checkGroupId(Long groupId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GROUP_ID)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check groupId!", e);
		}
	}

	private Group create(Group group) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_GROUP)) {
			statement.setString(1, group.getName());
			statement.setInt(2, group.getCourse());
			statement.setInt(3, group.getStream());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			group.setId(resultSet.getLong("group_id"));
		} catch (SQLException e) {
			throw new DAOException("Failed to create group!", e);
		}
		return group;
	}

	private Group update(Group group) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GROUP)) {
			statement.setString(1, group.getName());
			statement.setInt(2, group.getCourse());
			statement.setInt(3, group.getStream());
			statement.setLong(4, group.getId());
			statement.executeUpdate();
			return group;
		} catch (SQLException e) {
			throw new DAOException("Failed to update group!", e);
		}
	}

	private boolean checkStudentsByGroupId(Long groupId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_STUDENTS_BY_GROUP_ID)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check students by groupId!", e);
		}
	}

}
