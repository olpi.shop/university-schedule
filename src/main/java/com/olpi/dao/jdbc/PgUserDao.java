package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import com.olpi.dao.UserDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.exception.RegistrationExeption;
import com.olpi.dao.pool.ConnectionPool;
import com.olpi.dao.pool.ProxyConnection;
import com.olpi.dao.pool.exception.PoolException;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;

public class PgUserDao implements UserDao {

	private static final PgUserDao INSTANCE = new PgUserDao();

	private static final String SQL_SELECT_USER = "SELECT user_email, user_id, user_role FROM users WHERE user_email = ? AND user_pass = ?;";
	private static final String SQL_SELECT_USER_EMAIL = "SELECT user_id, user_role FROM users WHERE user_email = ?;";
	private static final String SQL_SELECT_USER_ID = "SELECT user_email, user_id, user_role FROM users WHERE user_id = ? AND user_role = ?::user_role;";
	private static final String SQL_INSERT_NEW_USER = "INSERT INTO users(user_email, user_pass, user_id, user_role) VALUES(?,?, ?, ?::user_role);";
	private static final String SQL_DELETE_USER = "DELETE FROM users WHERE user_email = ?;";
	private static final String SQL_DELETE_USER_BY_ROLE_AND_ID = "DELETE FROM users WHERE user_id = ? AND user_role = ?::user_role;";
	private static final String SQL_UPDATE_USER = "UPDATE users SET user_email = ?, user_pass = ? WHERE user_id = ? AND user_role = ?::user_role";
	private static final String SQL_UPDATE_USER_EMAIL = "UPDATE users SET user_email = ? WHERE user_id = ? AND user_role = ?::user_role";
	private static final String SQL_UPDATE_USER_PASSWORD = "UPDATE users SET user_pass = ? WHERE user_email = ? AND user_id = ? AND user_role = ?::user_role";

	private PgUserDao() {

	}

	public static PgUserDao getInstance() {
		return INSTANCE;
	}

	@Override
	public Optional<User> authorize(String email, String codedPassword) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER)) {
			statement.setString(1, email);
			statement.setString(2, codedPassword);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					return Optional.empty();
				}
				Long userId = resultSet.getLong("user_id");
				UserRole role = UserRole.valueOf(resultSet.getString("user_role"));
				User user = new User(email, userId, role);

				return Optional.of(user);
			}
		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while authoririze!", e);
		}
	}

	@Override
	public User register(String email, Long id, UserRole role, String codedPassword)
			throws DAOException, RegistrationExeption {
		if (findUserByEmail(email).isPresent()) {
			throw new RegistrationExeption("This e-mail already exist!");
		}
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_USER)) {
			statement.setString(1, email);
			statement.setString(2, codedPassword);
			statement.setLong(3, id);
			statement.setString(4, role.name());
			statement.executeUpdate();
			return new User(email, id, role);

		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while registration!", e);
		}
	}

	@Override
	public Optional<User> findUserByEmail(String email) throws DAOException {

		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER_EMAIL)) {
			statement.setString(1, email);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					return Optional.empty();
				}
				Long userId = resultSet.getLong("user_id");
				UserRole role = UserRole.valueOf(resultSet.getString("user_role"));
				User user = new User(email, userId, role);
				return Optional.of(user);
			}
		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while find User by e-mail!", e);
		}
	}

	@Override
	public Optional<User> findUserById(Long id, UserRole role) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER_ID)) {
			statement.setLong(1, id);
			statement.setString(2, role.name());
			try (ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					return Optional.empty();
				}
				String email = resultSet.getString("user_email");
				User user = new User(email, id, role);
				return Optional.of(user);
			}
		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while find User by id and role!", e);
		}
	}

	@Override
	public void removeUser(String email) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER)) {
			statement.setString(1, email);
			statement.executeUpdate();
		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while delete user!", e);
		}
	}

	@Override
	public void removeUser(Long id, UserRole role) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER_BY_ROLE_AND_ID)) {
			statement.setLong(1, id);
			statement.setString(2, role.name());
			statement.executeUpdate();
		} catch (SQLException | PoolException e) {
			throw new DAOException("Failed to access the database while delete user!", e);
		}
	}

	@Override
	public User updateUser(User user, String newEmail, String newCodedPassword) throws DAOException {
		if (findUserByEmail(user.getEmail()).isPresent()) {
			if (findUserByEmail(newEmail).isPresent()) {
				throw new DAOException("This new e-mail already exist!");
			}
			try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
					PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER)) {
				statement.setString(1, newEmail);
				statement.setString(2, newCodedPassword);
				statement.setLong(3, user.getId());
				statement.setString(4, user.getRole().name());
				statement.executeUpdate();
				return user;
			} catch (SQLException | PoolException e) {
				throw new DAOException("Failed to access the database while update user!", e);
			}

		} else {
			throw new DAOException("This user is not exist in DB!");
		}
	}

	@Override
	public User updateUserEmail(User user, String newEmail) throws DAOException {
		if (findUserById(user.getId(), user.getRole()).isPresent()) {
			if (findUserByEmail(newEmail).isPresent()) {
				throw new DAOException("This new e-mail already exist!");
			} 
			try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
					PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER_EMAIL)) {
				statement.setString(1, newEmail);
				statement.setLong(2, user.getId());
				statement.setString(3, user.getRole().name());
				statement.executeUpdate();
				return user;
			} catch (SQLException | PoolException e) {
				throw new DAOException("Failed to access the database while update user email!", e);
			}

		} else {
			throw new DAOException("There is not an user with this ID and Role!");
		}
	}

	public User updateUserPassword(User user, String newCodedPassword) throws DAOException {
		if (findUserById(user.getId(), user.getRole()).isPresent() && findUserByEmail(user.getEmail()).isPresent()) {
			try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
					PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER_PASSWORD)) {
				statement.setString(1, newCodedPassword);
				statement.setString(2, user.getEmail());
				statement.setLong(3, user.getId());
				statement.setString(4, user.getRole().name());
				statement.executeUpdate();
				return user;
			} catch (SQLException | PoolException e) {
				throw new DAOException("Failed to access the database while update user email!", e);
			}
		} else {
			throw new DAOException("There is not an user with this ID, e-mail and Role!");
		}
	}

}
