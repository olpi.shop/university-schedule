package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.postgresql.util.PSQLException;

import com.olpi.dao.WeekDayDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Group;
import com.olpi.domain.Lesson;
import com.olpi.domain.Schedule;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;
import com.olpi.domain.WeekDaySchedule;
import com.olpi.domain.enums.AcademicHour;
import com.olpi.domain.enums.Classroom;
import com.olpi.domain.enums.Department;

public class PgWeekDayDao extends WeekDayDao {

	private static final String SQL_SELECT_DAY_ID = "SELECT day_id FROM day_schedule WHERE day_id = ?::day_of_week;";
	private static final String SQL_SELECT_DAY_AND_SCHEDULE = "SELECT day_id, schedule_id FROM day_schedule WHERE day_id = ?::day_of_week AND schedule_id = ?";
	private static final String SQL_INSERT_SCHEDULE_IN_DAY = "INSERT INTO day_schedule (day_id, schedule_id) VALUES (?::day_of_week, ?);";
	private static final String SQL_DELETE_DAY = "DELETE FROM day_schedule WHERE day_id = ?::day_of_week;";
	private static final String SQL_DELETE_ALL_DAY = "DELETE FROM day_schedule;";
	private static final String SQL_SELECT_ALL_DAYS_OF_WEEK = "SELECT day_id FROM day_schedule;";
	private static final String SQL_SELECT_SCHEDULES_BY_DAYID = "SELECT schedules.schedule_id, " + "lessons.lesson_id, "
			+ "groups.group_id, groups.group_name, groups.course, groups.stream, "
			+ "subjects.subject_id, subjects.subject_name, subjects.department as subjectDepartment, "
			+ "teachers.teacher_id, teachers.first_name, teachers.last_name, teachers.department  as teacherDepartment, "
			+ "schedules.class_room, schedules.academic_hour " + "FROM day_schedule "
			+ "INNER JOIN schedules ON day_schedule.schedule_id = schedules.schedule_id "
			+ "INNER JOIN lessons ON schedules.lesson_id = lessons.lesson_id "
			+ "INNER JOIN groups ON lessons.group_id = groups.group_id "
			+ "INNER JOIN subjects ON lessons.subject_id = subjects.subject_id "
			+ "INNER JOIN teachers ON lessons.teacher_id = teachers.teacher_id " + "WHERE day_id = ?::day_of_week;";

	@Override
	public WeekDaySchedule createOrUpdate(WeekDaySchedule daySchedule) throws DAOException {
		if (!checkDayId(daySchedule.getDayId())) {
			return create(daySchedule);
		} else {
			return update(daySchedule);
		}
	}

	@Override
	public Optional<WeekDaySchedule> findById(DayOfWeek dayId) throws DAOException {
		WeekDaySchedule daySchedule = new WeekDaySchedule(dayId);
		if (!checkDayId(dayId)) {
			return Optional.empty();
		}
		List<Schedule> schedules = findSchedulesByDay(dayId);
		daySchedule.setSchedules(schedules);
		return Optional.of(daySchedule);
	}

	@Override
	public List<WeekDaySchedule> findAll() throws DAOException {
		List<WeekDaySchedule> daySchedules = findAllDays();
		for (WeekDaySchedule daySchedule : daySchedules) {
			List<Schedule> schedules = findSchedulesByDay(daySchedule.getDayId());
			daySchedule.setSchedules(schedules);
		}
		return daySchedules;
	}

	@Override
	public boolean remove(DayOfWeek dayId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_DAY)) {
			statement.setString(1, dayId.name());
			statement.executeUpdate();
			return !checkDayId(dayId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove day!", e);
		}
	}

	@Override
	public void removeAll() throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_DAY)) {
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to remove all days!", e);
		}
	}

	@Override
	public void addScheduleInDay(DayOfWeek dayId, Schedule schedule) throws DAOException {
		if (schedule.getId() == null) {
			throw new DAOException("The schedule doesn't have an ID! ");
		}
		addSchedule(dayId, schedule);
	}

	@Override
	public void addScheduleInDay(DayOfWeek dayId, List<Schedule> schedules) throws DAOException {
		for (Schedule schedule : schedules) {
			if (schedule.getId() == null) {
				throw new DAOException("The schedule doesn't have an ID! ");
			}
			addSchedule(dayId, schedule);
		}
	}

	private boolean checkDayId(DayOfWeek dayId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DAY_ID)) {
			statement.setString(1, dayId.name());
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check dayId!", e);
		}
	}

	private WeekDaySchedule create(WeekDaySchedule daySchedule) throws DAOException {
		addScheduleInDay(daySchedule.getDayId(), daySchedule.getSchedule());
		return daySchedule;
	}

	private WeekDaySchedule update(WeekDaySchedule daySchedule) throws DAOException {
		remove(daySchedule.getDayId());
		return create(daySchedule);
	}

	private void addSchedule(DayOfWeek dayId, Schedule schedule) throws DAOException {
		if (!checkWeekSchedule(dayId, schedule.getId())) {
			try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SCHEDULE_IN_DAY)) {
				statement.setString(1, dayId.name());
				statement.setLong(2, schedule.getId());
				statement.executeUpdate();
			} catch (PSQLException ex) {
				throw new DAOException("The schedule has an ID but there is not this id in the DB!", ex);
			} catch (SQLException e) {
				throw new DAOException("Failed to add schedule in day!", e);
			}
		}
	}

	private boolean checkWeekSchedule(DayOfWeek dayId, Long scheduleId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DAY_AND_SCHEDULE)) {
			statement.setString(1, dayId.name());
			statement.setLong(2, scheduleId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check dayId!", e);
		}
	}

	private List<WeekDaySchedule> findAllDays() throws DAOException {
		List<WeekDaySchedule> daySchedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_DAYS_OF_WEEK)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				DayOfWeek dayId = DayOfWeek.valueOf(resultSet.getString("day_id"));
				daySchedules.add(new WeekDaySchedule(dayId));
			}
			return daySchedules;
		} catch (SQLException e) {
			throw new DAOException("Failed to find all days!", e);
		}
	}

	private List<Schedule> findSchedulesByDay(DayOfWeek dayId) throws DAOException {
		List<Schedule> schedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SCHEDULES_BY_DAYID)) {
			statement.setString(1, dayId.name());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long groupId = resultSet.getLong("group_id");
				String groupName = resultSet.getString("group_name");
				int groupCourse = resultSet.getInt("course");
				int groupStream = resultSet.getInt("stream");
				Group group = new Group.Builder().id(groupId).name(groupName).course(groupCourse).stream(groupStream)
						.build();

				Long subjectId = resultSet.getLong("subject_id");
				String subjectName = resultSet.getString("subject_name");
				Department subjectDepartment = Department.valueOf(resultSet.getString("subjectDepartment"));
				Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(subjectDepartment)
						.build();

				Long teacherId = resultSet.getLong("teacher_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Department teacherDepartment = Department.valueOf(resultSet.getString("teacherDepartment"));
				Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
						.deparment(teacherDepartment).build();

				Long lessonId = resultSet.getLong("lesson_id");
				Lesson lesson = new Lesson.Builder().id(lessonId).group(group).subject(subject).teacher(teacher)
						.build();

				Long scheduleId = resultSet.getLong("schedule_id");
				Classroom room = Classroom.valueOf(resultSet.getString("class_room"));
				AcademicHour hour = AcademicHour.valueOf(resultSet.getString("academic_hour"));
				Schedule schedule = new Schedule.Builder().id(scheduleId).lesson(lesson).room(room).hour(hour).build();
				schedules.add(schedule);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find day-schedule!", e);
		}
		return schedules;
	}

}
