package com.olpi.dao.jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.postgresql.util.PSQLException;

import com.olpi.dao.DateScheduleDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.DateSchedule;
import com.olpi.domain.Group;
import com.olpi.domain.Lesson;
import com.olpi.domain.Schedule;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.AcademicHour;
import com.olpi.domain.enums.Classroom;
import com.olpi.domain.enums.Department;

public class PgDateScheduleDao extends DateScheduleDao {

	private static final String SQL_INSERT_SCHEDULE_IN_DATA = "INSERT INTO date_schedule (date_id, schedule_id) VALUES (? ,?);";
	private static final String SQL_SELECT_DATE_AND_SCHEDULE = "SELECT date_id, schedule_id FROM date_schedule WHERE date_id = ? AND schedule_id = ?; ";
	private static final String SQL_SELECT_DATE_ID = "SELECT date_id FROM date_schedule WHERE date_id = ?;";
	private static final String SQL_SELECT_ALL_DATES = "SELECT DISTINCT date_id FROM date_schedule;";
	private static final String SQL_DELETE_DATE = "DELETE FROM date_schedule WHERE date_id = ?;";
	private static final String SQL_DELETE_ALL_DATE = "DELETE FROM date_schedule;";
	private static final String SQL_SELECT_SCHEDULES = "SELECT schedules.schedule_id, " + "lessons.lesson_id, "
			+ "groups.group_id, groups.group_name, groups.course, groups.stream, "
			+ "subjects.subject_id, subjects.subject_name, subjects.department as subjectDepartment, "
			+ "teachers.teacher_id, teachers.first_name, teachers.last_name, teachers.department as teacherDepartment, "
			+ "schedules.class_room, schedules.academic_hour " + "FROM date_schedule "
			+ "INNER JOIN schedules ON date_schedule.schedule_id = schedules.schedule_id "
			+ "INNER JOIN lessons ON schedules.lesson_id = lessons.lesson_id "
			+ "INNER JOIN groups ON lessons.group_id = groups.group_id "
			+ "INNER JOIN subjects ON lessons.subject_id = subjects.subject_id "
			+ "INNER JOIN teachers ON lessons.teacher_id = teachers.teacher_id ";
	private static final String SQL_SELECT_SCHEDULES_BY_DATEID = SQL_SELECT_SCHEDULES + "WHERE date_id = ?;";
	private static final String SQL_SELECT_DATE_SCHEDULE_BY_GROUPID = SQL_SELECT_SCHEDULES
			+ "WHERE date_id = ? AND groups.group_id = ?;";
	private static final String SQL_SELECT_DATE_SCHEDULE_BY_TEACHERID = SQL_SELECT_SCHEDULES
			+ "WHERE date_id = ? AND teachers.teacher_id = ?;";

	@Override
	public DateSchedule createOrUpdate(DateSchedule dateSchedule) throws DAOException {
		if (!checkDateId(dateSchedule.getDateId())) {
			return create(dateSchedule);
		} else {
			return update(dateSchedule);
		}
	}

	@Override
	public Optional<DateSchedule> findById(LocalDate dateId) throws DAOException {
		DateSchedule dateSchedule = new DateSchedule(dateId);
		if (!checkDateId(dateId)) {
			return Optional.empty();
		}
		List<Schedule> schedules = findSchedulesByDate(dateId);
		dateSchedule.setSchedules(schedules);
		return Optional.of(dateSchedule);

	}

	@Override
	public List<DateSchedule> findAll() throws DAOException {
		List<DateSchedule> dateSchedules = findAllDateSchedules();
		for (DateSchedule dateSchedule : dateSchedules) {
			List<Schedule> schedules = findSchedulesByDate(dateSchedule.getDateId());
			dateSchedule.setSchedules(schedules);
		}
		return dateSchedules;
	}

	@Override
	public boolean remove(LocalDate dateId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_DATE)) {
			statement.setDate(1, Date.valueOf(dateId));
			statement.executeUpdate();
			return !checkDateId(dateId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove date!", e);
		}
	}

	@Override
	public void removeAll() throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_DATE)) {
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to remove all dates!", e);
		}
	}

	@Override
	public List<LocalDate> findAllDates() throws DAOException {
		List<LocalDate> dates = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_DATES)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				dates.add(resultSet.getDate("date_id").toLocalDate());
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all dates!", e);
		}
		return dates;
	}

	@Override
	public DateSchedule findScheduleByGroupId(LocalDate dateId, Long groupId) throws DAOException {
		DateSchedule dateSchedule = new DateSchedule(dateId);
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DATE_SCHEDULE_BY_GROUPID)) {
			statement.setDate(1, Date.valueOf(dateId));
			statement.setLong(2, groupId);
			ResultSet resultSet = statement.executeQuery();
			List<Schedule> schedules = readSchedules(resultSet);
			dateSchedule.setSchedules(schedules);
		} catch (SQLException e) {
			throw new DAOException("Failed to find date-schedule by groupId!", e);
		}
		return dateSchedule;
	}

	@Override
	public DateSchedule findScheduleByTeacherId(LocalDate dateId, Long teacherId) throws DAOException {
		DateSchedule dateSchedule = new DateSchedule(dateId);
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DATE_SCHEDULE_BY_TEACHERID)) {
			statement.setDate(1, Date.valueOf(dateId));
			statement.setLong(2, teacherId);
			ResultSet resultSet = statement.executeQuery();

			List<Schedule> schedules = readSchedules(resultSet);
			dateSchedule.setSchedules(schedules);
		} catch (SQLException e) {
			throw new DAOException("Failed to find date-schedule by teacherId!", e);
		}
		return dateSchedule;
	}

	@Override
	public void addScheduleByDate(LocalDate dateId, Schedule schedule) throws DAOException {
		if (!checkDateSchedule(dateId, schedule.getId())) {
			try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SCHEDULE_IN_DATA)) {
				statement.setDate(1, Date.valueOf(dateId));
				statement.setLong(2, schedule.getId());
				statement.executeUpdate();
			} catch (PSQLException ex) {
				throw new DAOException("The schedule has an ID but there is not this id in the DB!", ex);
			} catch (SQLException e) {
				throw new DAOException("Failed to add schedule in date!", e);
			}
		}
	}

	@Override
	public void addScheduleByDate(LocalDate dateId, List<Schedule> schedules) throws DAOException {
		for (Schedule schedule : schedules) {
			addScheduleByDate(dateId, schedule);
		}
	}

	private boolean checkDateSchedule(LocalDate dateId, Long scheduleId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DATE_AND_SCHEDULE)) {
			statement.setDate(1, Date.valueOf(dateId));
			statement.setLong(2, scheduleId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check dayId!", e);
		}
	}

	private boolean checkDateId(LocalDate dateId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_DATE_ID)) {
			statement.setDate(1, Date.valueOf(dateId));
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check dateId!", e);
		}
	}

	private DateSchedule create(DateSchedule dateSchedule) throws DAOException {
		addScheduleByDate(dateSchedule.getDateId(), dateSchedule.getSchedules());
		return dateSchedule;
	}

	private DateSchedule update(DateSchedule dateSchedule) throws DAOException {
		remove(dateSchedule.getDateId());
		return create(dateSchedule);
	}

	private List<DateSchedule> findAllDateSchedules() throws DAOException {
		List<DateSchedule> dateSchedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_DATES)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LocalDate dateId = resultSet.getDate("date_id").toLocalDate();
				dateSchedules.add(new DateSchedule(dateId));
			}
			return dateSchedules;
		} catch (SQLException e) {
			throw new DAOException("Failed to find all dates!", e);
		}
	}

	private List<Schedule> findSchedulesByDate(LocalDate dateId) throws DAOException {
		List<Schedule> schedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SCHEDULES_BY_DATEID)) {
			statement.setDate(1, Date.valueOf(dateId));
			ResultSet resultSet = statement.executeQuery();
			schedules = readSchedules(resultSet);
		} catch (SQLException e) {
			throw new DAOException("Failed to find date-schedule!", e);
		}
		return schedules;
	}

	private List<Schedule> readSchedules(ResultSet resultSet) throws SQLException {
		List<Schedule> schedules = new ArrayList<>();
		while (resultSet.next()) {
			Long groupId = resultSet.getLong("group_id");
			String groupName = resultSet.getString("group_name");
			int groupCourse = resultSet.getInt("course");
			int groupStream = resultSet.getInt("stream");
			Group group = new Group.Builder().id(groupId).name(groupName).course(groupCourse).stream(groupStream)
					.build();
			Long subjectId = resultSet.getLong("subject_id");
			String subjectName = resultSet.getString("subject_name");
			Department subjectDepartment = Department.valueOf(resultSet.getString("subjectDepartment"));
			Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(subjectDepartment)
					.build();
			Long teacherId = resultSet.getLong("teacher_id");
			String firstName = resultSet.getString("first_name");
			String lastName = resultSet.getString("last_name");
			Department teacherDepartment = Department.valueOf(resultSet.getString("teacherDepartment"));
			Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
					.deparment(teacherDepartment).build();
			Long lessonId = resultSet.getLong("lesson_id");
			Lesson lesson = new Lesson.Builder().id(lessonId).group(group).subject(subject).teacher(teacher).build();
			Long scheduleId = resultSet.getLong("schedule_id");
			Classroom room = Classroom.valueOf(resultSet.getString("class_room"));
			AcademicHour hour = AcademicHour.valueOf(resultSet.getString("academic_hour"));
			Schedule schedule = new Schedule.Builder().id(scheduleId).lesson(lesson).room(room).hour(hour).build();
			schedules.add(schedule);
		}
		return schedules;
	}

}
