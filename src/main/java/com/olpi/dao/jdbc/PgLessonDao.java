package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.LessonDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Group;
import com.olpi.domain.Lesson;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.Department;

public class PgLessonDao extends LessonDao {

	private static final String SQL_INSERT_NEW_LESSON = "INSERT INTO lessons (group_id, subject_id, teacher_id) VALUES (?,?,?) RETURNING lesson_id;";
	private static final String SQL_SELECT_LESSON_ID = "SELECT lesson_id FROM lessons WHERE lesson_id = ?;";
	private static final String SQL_UPDATE_LESSON = "UPDATE  lessons SET group_id = ?, subject_id = ?, teacher_id = ? WHERE lesson_id = ?;";
	private static final String SQL_SELECT_ALL_LESSONS = "SELECT lessons.lesson_id, "
			+ "groups.group_id, groups.group_name, groups.course, groups.stream, "
			+ "subjects.subject_id, subjects.subject_name, subjects.department as subjectDepartment, "
			+ "teachers.teacher_id, teachers.first_name, teachers.last_name, teachers.department as teacherDepartment FROM lessons "
			+ "INNER JOIN groups ON lessons.group_id = groups.group_id "
			+ "INNER JOIN subjects ON lessons.subject_id = subjects.subject_id "
			+ "INNER JOIN teachers ON lessons.teacher_id = teachers.teacher_id ";
	private static final String SQL_SELECT_LESSON = SQL_SELECT_ALL_LESSONS + " WHERE lesson_id = ?;";
	private static final String SQL_DELETE_LESSON = "DELETE FROM lessons WHERE lesson_id = ?;";
	private static final String SQL_UPDATE_TEACHER_ID_IN_LESSON = "UPDATE  lessons SET teacher_id = ? WHERE lesson_id = ?;";
	private static final String SQL_DELETE_ALL_LESSONS = "DELETE FROM lessons";

	@Override
	public Lesson createOrUpdate(Lesson lesson) throws DAOException {
		if (lesson.getId() == null) {
			return create(lesson);
		} else if (checkLessonId(lesson.getId())) {
			return update(lesson);
		} else {
			throw new DAOException("The lesson has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Lesson> findById(Long lessonId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LESSON)) {
			statement.setLong(1, lessonId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			Long groupId = resultSet.getLong("group_id");
			String groupName = resultSet.getString("group_name");
			int groupCourse = resultSet.getInt("course");
			int groupStream = resultSet.getInt("stream");
			Group group = new Group.Builder().id(groupId).name(groupName).course(groupCourse).stream(groupStream)
					.build();

			Long subjectId = resultSet.getLong("subject_id");
			String subjectName = resultSet.getString("subject_name");
			Department subjectDepartment = Department.valueOf(resultSet.getString("subjectDepartment"));
			Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(subjectDepartment)
					.build();

			Long teacherId = resultSet.getLong("teacher_id");
			String firstName = resultSet.getString("first_name");
			String lastName = resultSet.getString("last_name");
			Department teacherDepartment = Department.valueOf(resultSet.getString("teacherDepartment"));
			Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
					.deparment(teacherDepartment).build();

			Lesson lesson = new Lesson.Builder().id(lessonId).group(group).subject(subject).teacher(teacher).build();
			return Optional.of(lesson);
		} catch (SQLException e) {
			throw new DAOException("Failed to find lesson by id!", e);
		}
	}

	@Override
	public List<Lesson> findAll() throws DAOException {
		List<Lesson> lessons = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_LESSONS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {

				Long groupId = resultSet.getLong("group_id");
				String groupName = resultSet.getString("group_name");
				int groupCourse = resultSet.getInt("course");
				int groupStream = resultSet.getInt("stream");
				Group group = new Group.Builder().id(groupId).name(groupName).course(groupCourse).stream(groupStream)
						.build();

				Long subjectId = resultSet.getLong("subject_id");
				String subjectName = resultSet.getString("subject_name");
				Department subjectDepartment = Department.valueOf(resultSet.getString("subjectDepartment"));
				Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(subjectDepartment)
						.build();

				Long teacherId = resultSet.getLong("teacher_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Department teacherDepartment = Department.valueOf(resultSet.getString("teacherDepartment"));
				Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
						.deparment(teacherDepartment).build();

				Long lessonId = resultSet.getLong("lesson_id");

				Lesson lesson = new Lesson.Builder().id(lessonId).group(group).subject(subject).teacher(teacher)
						.build();
				lessons.add(lesson);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all lessons!", e);
		}
		return lessons;
	}

	@Override
	public boolean remove(Long lessonId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_LESSON)) {
			statement.setLong(1, lessonId);
			statement.executeUpdate();
			return !checkLessonId(lessonId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove lesson!", e);
		}
	}

	@Override
	public void replaceTeacher(Long lessonId, Long newTeacherId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TEACHER_ID_IN_LESSON)) {
			statement.setLong(1, newTeacherId);
			statement.setLong(2, lessonId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to replace teacher id in lesson!", e);
		}
	}

	@Override
	public void removeAll() throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_LESSONS)) {
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to delete all lessons!", e);
		}
	}

	private Lesson create(Lesson lesson) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_LESSON)) {
			statement.setLong(1, lesson.getGroup().getId());
			statement.setLong(2, lesson.getSubject().getId());
			statement.setLong(3, lesson.getTeacher().getId());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			lesson.setId(resultSet.getLong("lesson_id"));

		} catch (SQLException e) {
			throw new DAOException("Failed to create lesson!", e);
		}
		return lesson;
	}

	private Lesson update(Lesson lesson) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_LESSON)) {
			statement.setLong(1, lesson.getGroup().getId());
			statement.setLong(2, lesson.getSubject().getId());
			statement.setLong(3, lesson.getTeacher().getId());
			statement.setLong(4, lesson.getId());
			statement.executeUpdate();
			return lesson;
		} catch (SQLException e) {
			throw new DAOException("Failed to update student!", e);
		}
	}

	private boolean checkLessonId(Long lessonId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LESSON_ID)) {
			statement.setLong(1, lessonId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check lessonId!", e);
		}
	}

}
