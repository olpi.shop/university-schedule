package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.ScheduleDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Group;
import com.olpi.domain.Lesson;
import com.olpi.domain.Schedule;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.AcademicHour;
import com.olpi.domain.enums.Classroom;
import com.olpi.domain.enums.Department;

public class PgScheduleDao extends ScheduleDao {

	private static final String SQL_INSERT_NEW_SCHEDULE = "INSERT INTO schedules (lesson_id, class_room, academic_hour) VALUES (?,?::class_room,?::academic_hour) RETURNING schedule_id;";
	private static final String SQL_SELECT_SCHEDULE_ID = "SELECT schedule_id FROM schedules WHERE schedule_id = ?;";
	private static final String SQL_UPDATE_SCHEDULE = "UPDATE  schedules SET lesson_id = ?, class_room = ?::class_room, academic_hour = ?::academic_hour WHERE schedule_id = ?;";
	private static final String SQL_SELECT_ALL_SCHEDULES = "SELECT schedules.schedule_id," + "lessons.lesson_id, "
			+ "groups.group_id, groups.group_name, groups.course, groups.stream, "
			+ "subjects.subject_id, subjects.subject_name, subjects.department as subjectDepartment, "
			+ "teachers.teacher_id, teachers.first_name, teachers.last_name, teachers.department as teacherDepartment, "
			+ "schedules.class_room, schedules.academic_hour " + "FROM schedules "
			+ "INNER JOIN lessons ON schedules.lesson_id = lessons.lesson_id "
			+ "INNER JOIN groups ON lessons.group_id = groups.group_id "
			+ "INNER JOIN subjects ON lessons.subject_id = subjects.subject_id "
			+ "INNER JOIN teachers ON lessons.teacher_id = teachers.teacher_id ";
	private static final String SQL_SELECT_SCHEDULE = SQL_SELECT_ALL_SCHEDULES + "WHERE schedule_id = ?;";
	private static final String SQL_SELECT_SCHEDULES_BY_GROUP_ID = SQL_SELECT_ALL_SCHEDULES
			+ "WHERE groups.group_id = ?";
	private static final String SQL_DELETE_SCHEDULE = "DELETE FROM schedules WHERE schedule_id  = ?;";
	private static final String SQL_DELETE_ALL_SCHEDULES = "DELETE FROM schedules";

	@Override
	public Schedule createOrUpdate(Schedule schedule) throws DAOException {
		if (schedule.getId() == null) {
			return create(schedule);
		} else if (checkScheduleId(schedule.getId())) {
			return update(schedule);
		} else {
			throw new DAOException("The schedule has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Schedule> findById(Long scheduleId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SCHEDULE)) {
			statement.setLong(1, scheduleId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			Schedule schedule = readSchedule(resultSet);
			return Optional.of(schedule);
		} catch (SQLException e) {
			throw new DAOException("Failed to find schedule by id!", e);
		}
	}

	@Override
	public List<Schedule> findAll() throws DAOException {
		List<Schedule> schedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_SCHEDULES)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				schedules.add(readSchedule(resultSet));
			}
			return schedules;
		} catch (SQLException e) {
			throw new DAOException("Failed to find all schedules!", e);
		}
	}

	@Override
	public boolean remove(Long scheduleId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_SCHEDULE)) {
			statement.setLong(1, scheduleId);
			statement.executeUpdate();
			return !checkScheduleId(scheduleId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove schedule!", e);
		}
	}

	@Override
	public void removeAll() throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_SCHEDULES)) {
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to delete all schedules!", e);
		}
	}

	@Override
	public List<Schedule> findByGroupId(Long groupId) throws DAOException {
		List<Schedule> schedules = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SCHEDULES_BY_GROUP_ID)) {
			statement.setLong(1, groupId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				schedules.add(readSchedule(resultSet));
			}
			return schedules;
		} catch (SQLException e) {
			throw new DAOException("Failed to find schedules by groupId!", e);
		}
	}

	private Schedule create(Schedule schedule) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_SCHEDULE)) {
			statement.setLong(1, schedule.getLesson().getId());
			statement.setString(2, schedule.getRoom().name());
			statement.setString(3, schedule.getHour().name());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			schedule.setId(resultSet.getLong("schedule_id"));
			return schedule;
		} catch (SQLException e) {
			throw new DAOException("Failed to create schedule!", e);
		}
	}

	private Schedule update(Schedule schedule) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_SCHEDULE)) {
			statement.setLong(1, schedule.getLesson().getId());
			statement.setString(2, schedule.getRoom().name());
			statement.setString(3, schedule.getHour().name());
			statement.setLong(4, schedule.getId());
			statement.executeUpdate();
			return schedule;
		} catch (SQLException e) {
			throw new DAOException("Failed to schedule student!", e);
		}
	}

	private boolean checkScheduleId(Long scheduleId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SCHEDULE_ID)) {
			statement.setLong(1, scheduleId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check scheduleId!", e);
		}
	}

	private Schedule readSchedule(ResultSet resultSet) throws SQLException {
		Long groupId = resultSet.getLong("group_id");
		String groupName = resultSet.getString("group_name");
		int groupCourse = resultSet.getInt("course");
		int groupStream = resultSet.getInt("stream");
		Group group = new Group.Builder().id(groupId).name(groupName).course(groupCourse).stream(groupStream).build();
		Long subjectId = resultSet.getLong("subject_id");
		String subjectName = resultSet.getString("subject_name");
		Department subjectDepartment = Department.valueOf(resultSet.getString("subjectDepartment"));
		Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(subjectDepartment).build();
		Long teacherId = resultSet.getLong("teacher_id");
		String firstName = resultSet.getString("first_name");
		String lastName = resultSet.getString("last_name");
		Department teacherDepartment = Department.valueOf(resultSet.getString("teacherDepartment"));
		Teacher teacher = new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
				.deparment(teacherDepartment).build();
		Long lessonId = resultSet.getLong("lesson_id");
		Lesson lesson = new Lesson.Builder().id(lessonId).group(group).subject(subject).teacher(teacher).build();
		Long scheduleId = resultSet.getLong("schedule_id");
		Classroom room = Classroom.valueOf(resultSet.getString("class_room"));
		AcademicHour hour = AcademicHour.valueOf(resultSet.getString("academic_hour"));

		return new Schedule.Builder().id(scheduleId).lesson(lesson).room(room).hour(hour).build();
	}

}
