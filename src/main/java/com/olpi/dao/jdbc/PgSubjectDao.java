package com.olpi.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.SubjectDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.Department;

public class PgSubjectDao extends SubjectDao {

	private static final String SQL_INSERT_NEW_SUBJECT = "INSERT INTO subjects (subject_name, department) VALUES (?,?::department) RETURNING subject_id;";
	private static final String SQL_SELECT_SUBJECT_ID = "SELECT subject_id FROM subjects WHERE subject_id = ?;";
	private static final String SQL_UPDATE_SUBJECT = "UPDATE  subjects SET subject_name = ?, department = ?::department WHERE subject_id = ?;";
	private static final String SQL_DELETE_SUBJECT = "DELETE FROM subjects WHERE subject_id = ?;";
	private static final String SQL_SELECT_SUBJECT = "SELECT subject_name, department FROM subjects  WHERE subject_id = ?;";
	private static final String SQL_SELECT_ALL_SUBJECTS = "SELECT subject_id, subject_name, department FROM subjects;";
	private static final String SQL_SELECT_TEACHERS_BY_SYBJECTID = "SELECT teachers.teacher_id, teachers.first_name, teachers.last_name, teachers.department "
			+ "FROM subject_teacher "
			+ "INNER JOIN teachers ON subject_teacher.teacher_id = teachers.teacher_id WHERE subject_teacher.subject_id = ?;";
	private static final String SQL_INSERT_TEACHER_TO_SUBJECT = "INSERT INTO subject_teacher(subject_id, teacher_id) VALUES (?, ?);";
	private static final String SQL_DELETE_ALL_TEACHERS = "DELETE FROM subject_teacher WHERE teacher_id = ?;";

	@Override
	public Subject createOrUpdate(Subject subject) throws DAOException {
		if (subject.getId() == null) {
			return create(subject);
		} else if (checkSubjectId(subject.getId())) {
			return update(subject);
		} else {
			throw new DAOException("The subject has an ID but there is not the id in the DB!");
		}
	}

	@Override
	public Optional<Subject> findById(Long subjectId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SUBJECT)) {
			statement.setLong(1, subjectId);
			ResultSet resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return Optional.empty();
			}
			String subjectName = resultSet.getString("subject_name");
			Department department = Department.valueOf(resultSet.getString("department"));
			List<Teacher> teachers = findTeachersBySubjectId(subjectId);

			Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(department).build();
			subject.setTeachers(teachers);
			return Optional.of(subject);
		} catch (SQLException e) {
			throw new DAOException("Failed to find subject by id!", e);
		}
	}

	@Override
	public List<Subject> findAll() throws DAOException {
		List<Subject> subjects = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_SUBJECTS)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long subjectId = resultSet.getLong("subject_id");
				String subjectName = resultSet.getString("subject_name");
				Department department = Department.valueOf(resultSet.getString("department"));
				List<Teacher> teachers = findTeachersBySubjectId(subjectId);

				Subject subject = new Subject.Builder().id(subjectId).name(subjectName).deparment(department).build();
				subject.setTeachers(teachers);

				subjects.add(subject);
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all subjects!", e);
		}
		return subjects;
	}

	@Override
	public List<Teacher> findTeachersBySubjectId(Long subjectId) throws DAOException {
		List<Teacher> teachers = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TEACHERS_BY_SYBJECTID)) {
			statement.setLong(1, subjectId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Long teacherId = resultSet.getLong("teacher_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Department department = Department.valueOf(resultSet.getString("department"));

				teachers.add(new Teacher.Builder().id(teacherId).firstName(firstName).lastName(lastName)
						.deparment(department).build());
			}
		} catch (SQLException e) {
			throw new DAOException("Failed to find all students in group!", e);
		}
		return teachers;
	}

	@Override
	public boolean remove(Long subjectId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_SUBJECT)) {
			statement.setLong(1, subjectId);
			statement.executeUpdate();
			return !checkSubjectId(subjectId);
		} catch (SQLException e) {
			throw new DAOException("Failed to remove subject!", e);
		}
	}

	@Override
	public void addTeachersToSubject(List<Teacher> teachers, Long subjectId) throws DAOException {
		for (Teacher teacher : teachers) {
			try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_TEACHER_TO_SUBJECT)) {
				statement.setLong(1, subjectId);
				statement.setLong(2, teacher.getId());
				statement.executeUpdate();
			} catch (SQLException e) {
				throw new DAOException("Failed to add teachers in subject!", e);
			}
		}
	}

	@Override
	public void removeTeachersFromSubject(Long subjectId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_TEACHERS)) {
			statement.setLong(1, subjectId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Failed to delete teachers in subject!", e);
		}
	}

	private Subject create(Subject subject) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_SUBJECT)) {
			statement.setString(1, subject.getName());
			statement.setString(2, subject.getDepartment().name());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			subject.setId(resultSet.getLong("subject_id"));
		} catch (SQLException e) {
			throw new DAOException("Failed to create subject!", e);
		}
		return subject;
	}

	private Subject update(Subject subject) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_SUBJECT)) {
			statement.setString(1, subject.getName());
			statement.setString(2, subject.getDepartment().name());
			statement.setLong(3, subject.getId());
			statement.executeUpdate();
			removeTeachersFromSubject(subject.getId());
			addTeachersToSubject(subject.getTeachers(), subject.getId());
			return subject;
		} catch (SQLException e) {
			throw new DAOException("Failed to update subject!", e);
		}
	}

	private boolean checkSubjectId(Long subjectId) throws DAOException {
		try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SUBJECT_ID)) {
			statement.setLong(1, subjectId);
			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DAOException("Failed to check subjectId!", e);
		}
	}

}
