package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Teacher;
import com.olpi.domain.enums.Department;

public abstract class TeacherDao extends AbstractDao<Long, Teacher> {

	public abstract Teacher createOrUpdate(Teacher teacher) throws DAOException;

	public abstract Optional<Teacher> findById(Long teacherId) throws DAOException;

	public abstract List<Teacher> findAll() throws DAOException;

	public abstract List<Teacher> findAllByDepartment(Department department) throws DAOException;

	public abstract boolean remove(Long teacherId) throws DAOException;

	public abstract boolean checkTeacherId(Long id) throws DAOException;

}
