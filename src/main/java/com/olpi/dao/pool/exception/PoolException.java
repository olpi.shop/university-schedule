package com.olpi.dao.pool.exception;

@SuppressWarnings("serial")
public class PoolException extends Exception {

	public PoolException() {
		super();
	}

	public PoolException(String message) {
		super(message);
	}

	public PoolException(String message, Throwable cause) {
		super(message, cause);
	}

}
