package com.olpi.dao.pool;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.olpi.dao.pool.exception.PoolException;

public class ConnectionPool {

	private static final String PG_DATABASE_PROPERTY_PATH = "pg_database";
	private static final String PARAMETER_URL = "url";
	private static final String PARAMETER_USER = "user";
	private static final String PARAMETER_PASSWORD = "password";
	private static final String PARAMETER_CHARACTER_ENCODING = "characterEncoding";
	private static final String PARAMETER_USE_UNICODE = "useUnicode";
	private static final String PARAMETER_POLL_TIMEOUT = "pollTimeout";
	private static final String PARAMETER_AUTO_RECONNECT = "autoReconnect";

	private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
	private static ConnectionPool instance;
	private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
	private static ReentrantLock lock = new ReentrantLock();
	private BlockingQueue<ProxyConnection> busyConnections;
	private BlockingQueue<ProxyConnection> availableConnections;
	private Properties property;
	private String connectUrl;

	private static final int POOL_SIZE = 10;
	private static final String CHARACTER_ENCODING = "UTF-8";
	private static final boolean USE_UNICODE = true;
	private static final int POLL_TIMEOUT = 2000;
	private static final boolean AUTO_RECONNECT = true;

	private ConnectionPool() throws PoolException {
		initWithPgConnections();
	}

	private void initWithPgConnections() throws PoolException {
		availableConnections = new ArrayBlockingQueue<>(POOL_SIZE);
		busyConnections = new ArrayBlockingQueue<>(POOL_SIZE);

		property = new Properties();
		property.put(PARAMETER_CHARACTER_ENCODING, CHARACTER_ENCODING);
		property.put(PARAMETER_USE_UNICODE, USE_UNICODE);
		property.put(PARAMETER_POLL_TIMEOUT, POLL_TIMEOUT);
		property.put(PARAMETER_AUTO_RECONNECT, AUTO_RECONNECT);

		ResourceBundle resource = ResourceBundle.getBundle(PG_DATABASE_PROPERTY_PATH);
		connectUrl = resource.getString(PARAMETER_URL);
		property.put(PARAMETER_USER, resource.getString(PARAMETER_USER));
		property.put(PARAMETER_PASSWORD, resource.getString(PARAMETER_PASSWORD));
		try {
			DriverManager.registerDriver(new org.postgresql.Driver());
			for (int i = 0; i < POOL_SIZE; i++) {
				Connection connection = DriverManager.getConnection(connectUrl, property);
				ProxyConnection proxyConnection = new ProxyConnection(connection);
				availableConnections.add(proxyConnection);
			}
		} catch (SQLException ex) {
			throw new PoolException("Failed to initial pool!", ex);
		}
	}

	public static ConnectionPool getInstance() throws PoolException {
		if (!instanceCreated.get()) {
			lock.lock();
			try {
				if (instance == null) {
					instance = new ConnectionPool();
					instanceCreated.set(true);
					LOGGER.log(Level.DEBUG, "Connection pool has been initialized");
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	public ProxyConnection getConnection() throws PoolException {
		ProxyConnection pConnection = null;
		try {
			if (!instanceCreated.get()) {
				throw new PoolException("Connection pool does not initialized!");
			}
			pConnection = availableConnections.take();
			int timeout = 1;
			if (!pConnection.isValid(timeout)) {
				Connection connection = DriverManager.getConnection(connectUrl, property);
				pConnection.closeConnection();
				pConnection = new ProxyConnection(connection);
			}
			busyConnections.put(pConnection);
		} catch (InterruptedException ex) {
			LOGGER.log(Level.WARN, "Interrupted during get connection from the pool!", ex);
			Thread.currentThread().interrupt();
		} catch (SQLException ex) {
			throw new PoolException("Failed to get connection from the pool!", ex);
		}
		return pConnection;
	}

	public void realeseConnection(ProxyConnection connection) throws PoolException {
		try {
			if (connection.isClosed()) {
				throw new SQLException("Unable to realese closed connection");
			}
			connection.setAutoCommit(true);
			if (!busyConnections.remove(connection)) {
				connection.closeConnection();
			} else {
				availableConnections.put(connection);
			}
		} catch (SQLException ex) {
			throw new PoolException("Failed to realese connection in pool!", ex);
		} catch (InterruptedException e) {
			LOGGER.log(Level.WARN, "Interrupted during realese connection in pool!", e);
			Thread.currentThread().interrupt();
		}
	}

	public void destroy() throws PoolException {
		if (instanceCreated.get()) {
			lock.lock();
			try {
				for (ProxyConnection connection : availableConnections) {
					connection.closeConnection();
				}
				for (ProxyConnection connection : busyConnections) {
					connection.closeConnection();
				}
				busyConnections.clear();
				availableConnections.clear();
				instanceCreated.set(false);

				Enumeration<Driver> drivers = DriverManager.getDrivers();
				while (drivers.hasMoreElements()) {
					Driver driver = drivers.nextElement();
					DriverManager.deregisterDriver(driver);
				}
				LOGGER.log(Level.DEBUG, "Connection pool has been destroyed");
			} catch (SQLException e) {
				throw new PoolException("Failed to destroy connection pool", e);
			} finally {
				lock.unlock();
			}
		}
	}

}
