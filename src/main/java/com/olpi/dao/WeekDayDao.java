package com.olpi.dao;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Schedule;
import com.olpi.domain.WeekDaySchedule;

public abstract class WeekDayDao extends AbstractDao<DayOfWeek, WeekDaySchedule> {

	public abstract WeekDaySchedule createOrUpdate(WeekDaySchedule daySchedule) throws DAOException;

	public abstract Optional<WeekDaySchedule> findById(DayOfWeek dayId) throws DAOException;

	public abstract List<WeekDaySchedule> findAll() throws DAOException;

	public abstract boolean remove(DayOfWeek dayId) throws DAOException;

	public abstract void removeAll() throws DAOException;

	public abstract void addScheduleInDay(DayOfWeek dayId, Schedule schedule) throws DAOException;

	public abstract void addScheduleInDay(DayOfWeek dayId, List<Schedule> schedules) throws DAOException;

}
