package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Group;
import com.olpi.domain.Student;

public abstract class GroupDao extends AbstractDao<Long, Group> {

	public abstract Group createOrUpdate(Group group) throws DAOException;

	public abstract Optional<Group> findById(Long groupId) throws DAOException;

	public abstract List<Group> findAll() throws DAOException;

	public abstract Optional<Group> findGroupWithoutStudents(Long groupId) throws DAOException;

	public abstract List<Group> findAllGroupsWithoutStudents() throws DAOException;

	public abstract boolean remove(Long groupId) throws DAOException;

	public abstract List<Student> findStudentsByGroupId(Long groupId) throws DAOException;

	public abstract boolean checkGroupId(Long groupId) throws DAOException;

}
