package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Subject;
import com.olpi.domain.Teacher;

public abstract class SubjectDao extends AbstractDao<Long, Subject> {

	public abstract Subject createOrUpdate(Subject subject) throws DAOException;

	public abstract Optional<Subject> findById(Long subjectId) throws DAOException;

	public abstract List<Subject> findAll() throws DAOException;

	public abstract List<Teacher> findTeachersBySubjectId(Long subjectId) throws DAOException;

	public abstract void addTeachersToSubject(List<Teacher> teachers, Long subjectId) throws DAOException;

	public abstract boolean remove(Long subjectId) throws DAOException;

	public abstract void removeTeachersFromSubject(Long subjectId) throws DAOException;

}
