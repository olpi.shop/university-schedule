package com.olpi.dao;

import java.sql.SQLException;

import com.olpi.dao.exception.DAOException;
import com.olpi.dao.pool.ConnectionPool;
import com.olpi.dao.pool.ProxyConnection;
import com.olpi.dao.pool.exception.PoolException;

public class TransactionManager {
	private ProxyConnection connection;

	public TransactionManager() throws PoolException {
		connection = ConnectionPool.getInstance().getConnection();
	}

	@SuppressWarnings("rawtypes")
	public void beginTransaction(AbstractDao dao, AbstractDao... daos) {
		dao.setConnection(connection);
		for (AbstractDao aDao : daos) {
			aDao.setConnection(connection);
		}
	}

	public void setAutoCommit(boolean autoCommit) throws DAOException {
		try {
			connection.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			throw new DAOException("Failed to access the database while setAutoCommit!", e);
		}
	}

	public void commit() throws DAOException {
		try {
			connection.commit();
		} catch (SQLException e) {
			throw new DAOException("Failed to access the database while committing!", e);
		}
	}

	public void rollback() throws DAOException {
		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new DAOException("Failed to access the database while rollbacking!", e);
		}
	}

	public void endTransaction() throws PoolException {
		ConnectionPool.getInstance().realeseConnection(connection);
	}
}
