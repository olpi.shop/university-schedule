package com.olpi.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Entity;

public abstract class AbstractDao<K, T extends Entity> {

	protected Connection connection;

	public abstract T createOrUpdate(T entity) throws DAOException;

	public abstract Optional<T> findById(K id) throws DAOException;

	public abstract List<T> findAll() throws DAOException;

	public abstract boolean remove(K id) throws DAOException;

	void setConnection(Connection connection) {
		this.connection = connection;
	}

}
