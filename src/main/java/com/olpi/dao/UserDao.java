package com.olpi.dao;

import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.dao.exception.RegistrationExeption;
import com.olpi.domain.User;
import com.olpi.domain.enums.UserRole;

public interface UserDao {

	Optional<User> authorize(String email, String codedPassword) throws DAOException;

	User register(String email, Long id, UserRole role, String codedPassword) throws DAOException, RegistrationExeption;

	User updateUser(User user, String newEmail, String newCodedPassword) throws DAOException;

	User updateUserEmail(User user, String newEmail) throws DAOException;

	User updateUserPassword(User user, String newCodedPassword) throws DAOException;

	Optional<User> findUserByEmail(String email) throws DAOException;

	Optional<User> findUserById(Long id, UserRole role) throws DAOException;

	void removeUser(String email) throws DAOException;

	void removeUser(Long id, UserRole role) throws DAOException;

}
