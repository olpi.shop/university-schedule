package com.olpi.dao;

import java.util.List;
import java.util.Optional;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Schedule;

public abstract class ScheduleDao extends AbstractDao<Long, Schedule> {

	public abstract Schedule createOrUpdate(Schedule schedule) throws DAOException;

	public abstract Optional<Schedule> findById(Long scheduleId) throws DAOException;

	public abstract List<Schedule> findAll() throws DAOException;

	public abstract boolean remove(Long scheduleId) throws DAOException;

	public abstract void removeAll() throws DAOException;

	public abstract List<Schedule> findByGroupId(Long groupId) throws DAOException;

}
