DROP DATABASE IF EXISTS university_schedule;
DROP USER IF EXISTS admin;
CREATE USER admin WITH password '123';
CREATE DATABASE university_schedule WITH OWNER = admin  ENCODING = 'UTF-8' TABLESPACE = pg_default;
GRANT ALL ON DATABASE university_schedule TO admin;
