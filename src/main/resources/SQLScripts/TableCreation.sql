DROP TABLE IF EXISTS date_schedule;
DROP TABLE IF EXISTS day_schedule;

DROP TABLE IF EXISTS schedules;
DROP TABLE IF EXISTS lessons;

DROP TABLE IF EXISTS subject_teacher;

DROP TABLE IF EXISTS subjects;
DROP TABLE IF EXISTS teachers;

DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS users;

DROP TYPE IF EXISTS DAY_OF_WEEK;
DROP TYPE IF EXISTS DEPARTMENT;
DROP TYPE IF EXISTS ACADEMIC_HOUR;
DROP TYPE IF EXISTS CLASS_ROOM;
DROP TYPE IF EXISTS USER_ROLE;

CREATE TYPE USER_ROLE AS ENUM (
  'STUDENT', 
  'TEACHER', 
  'ADMIN'
);
ALTER TYPE USER_ROLE OWNER TO admin;

CREATE TYPE ACADEMIC_HOUR AS ENUM (
  'FIRST', 
  'SECOND', 
  'THIRD',
  'FOURTH',
  'FIVETH',
  'SIXTH',
  'SEVENTH',
  'EIGHTH',
  'NINETH'
);
ALTER TYPE ACADEMIC_HOUR OWNER TO admin;
  
CREATE TYPE CLASS_ROOM AS ENUM (
    'A101', 'B101', 'C101', 'D101', 'A102',
    'B102', 'C102', 'D102', 'A103', 'B103',
    'C103', 'D103', 'A104', 'B104', 'C104',
    'D104', 'A201', 'B201', 'C201', 'D201',
    'A202', 'B202', 'C202', 'D202', 'A203',
    'B203', 'C203', 'D203', 'A204', 'B204',
    'C204', 'D204', 'A301', 'B301', 'C301',
    'D301', 'A302', 'B302', 'C302', 'D302',
    'A303', 'B303', 'C303', 'D303', 'A304',
    'B304', 'C304', 'D304'
);
ALTER TYPE CLASS_ROOM OWNER TO admin;

CREATE TYPE DEPARTMENT AS ENUM (
  'ECONOMICS', 
  'ECOLOGY', 
  'PHILOSOPHY',
  'MATH',
  'INFORMATION_SYSTEMS'
);
ALTER TYPE DEPARTMENT OWNER TO admin;

CREATE TYPE DAY_OF_WEEK AS ENUM (
  'MONDAY', 
  'TUESDAY', 
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY',
  'SUNDAY'
);
ALTER TYPE DAY_OF_WEEK OWNER TO admin;

 CREATE TABLE users (
    user_email  VARCHAR(30) NOT NULL,
    user_pass VARCHAR(32) NOT NULL,
    user_id INT NOT NULL,
    user_role USER_ROLE NOT NULL,
    CONSTRAINT user_email_unique UNIQUE (user_email)
);
ALTER TABLE users OWNER TO admin;
INSERT INTO users(user_email, user_pass, user_id, user_role) VALUES('qq@qq.qq','b59c67bf196a4758191e42f76670ceba', '0', 'ADMIN');

 CREATE TABLE groups (
    group_id SERIAL PRIMARY KEY,
    group_name VARCHAR(30) NOT NULL,
    course INT NOT NULL,
    stream INT NOT NULL
);
ALTER TABLE groups OWNER TO admin;

CREATE TABLE students(
    student_id SERIAL PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    group_id INT NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
ALTER TABLE students OWNER TO admin;

CREATE TABLE teachers(
    teacher_id SERIAL PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    department DEPARTMENT NOT NULL
);
ALTER TABLE teachers OWNER TO admin;


CREATE TABLE subjects (
    subject_id SERIAL PRIMARY KEY,
    subject_name VARCHAR(30) NOT NULL,
    department DEPARTMENT NOT NULL
);
ALTER TABLE subjects OWNER TO admin;

CREATE TABLE subject_teacher (
    subject_id INT NOT NULL,
    teacher_id INT NOT NULL,
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id) ON DELETE CASCADE ON UPDATE CASCADE, 
    FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id) ON DELETE CASCADE ON UPDATE CASCADE
);
ALTER TABLE subject_teacher OWNER TO admin;

CREATE TABLE lessons(
    lesson_id SERIAL PRIMARY KEY,
    group_id INT NOT NULL,
    subject_id INT NOT NULL,
    teacher_id INT NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE ON UPDATE CASCADE, 
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
ALTER TABLE lessons OWNER TO admin;

CREATE TABLE schedules(
    schedule_id SERIAL PRIMARY KEY,
    lesson_id INT  NOT NULL,
    class_room CLASS_ROOM NOT NULL, 
    academic_hour ACADEMIC_HOUR NOT NULL,
    FOREIGN KEY (lesson_id) REFERENCES lessons(lesson_id) ON DELETE CASCADE ON UPDATE CASCADE
);
ALTER TABLE schedules OWNER TO admin;

CREATE TABLE day_schedule(
    day_id DAY_OF_WEEK NOT NULL, 
    schedule_id INT NOT NULL,
    FOREIGN KEY (schedule_id) REFERENCES schedules(schedule_id) ON DELETE CASCADE ON UPDATE CASCADE
);
ALTER TABLE day_schedule OWNER TO admin;

CREATE TABLE date_schedule(
    date_id DATE NOT NULL, 
    schedule_id INT NOT NULL,
    FOREIGN KEY (schedule_id) REFERENCES schedules(schedule_id) ON DELETE CASCADE ON UPDATE CASCADE
);
ALTER TABLE date_schedule OWNER TO admin;
